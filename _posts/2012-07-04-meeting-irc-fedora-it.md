---
categories:
- staff news
layout: ultime_news
title: 'Meeting IRC - #fedora-it'
created: 1341433862
---
<p>
	Il prossimo meeting IRC, come riportato anche nella pagina dedicata su <a href="http://doc.fedoraonline.it/Meeting_IRC">http://doc.fedoraonline.it/Meeting_IRC</a> &egrave; previsto per</p>
<p class="rtecenter">
	<strong>gioved&igrave; 12 luglio ore 22:00</strong></p>
<p>
	Agenda prevista:</p>
<ul>
	<li>
		Aggiornamento Boutique</li>
	<li>
		Il team Localization (L10N - Traduzioni)</li>
</ul>
<p>
	Come sempre l&#39;incontro si svolger&agrave; sul canale #fedora-it di freenode e sar&agrave; logged. La durata prevista &egrave; di un&#39;ora.<br />
	Ulteriori dettagli si trovano sulla pagina del wiki.</p>
