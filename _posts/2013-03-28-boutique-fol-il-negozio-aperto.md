---
categories:
- staff news
layout: ultime_news
title: 'Boutique FOL: il negozio è aperto!'
created: 1364459730
---
<p>Eccoci con un&#39;altra sezione per la quale ci abbiamo messo un po&#39;, ma meglio tardi che mai :)<br />
	La Boutique Fedora Online &egrave; stata realizzata in collaborazione con gli amici francesi <a href="http://fedora-fr.org">http://fedora-fr.org</a>, i quali si sono resi disponibili, con la nostra partecipazione, a fare l&#39;upgrade dell&#39;account, tramite il quale d&#39;ora in poi possiamo mettere a disposizione tutti i gadget marchiati Fedora.<br />
	Abbiamo le stesse condizioni di vendita e spedizione degli utenti francesi e il negozietto &egrave; gi&agrave; tradotto al 90% in italiano.</p>
<p>La boutique &egrave; e rimane un servizio che semplicemente vogliamo mettere a disposizione di tutti, purtroppo non possiamo e non vogliamo addentrarci di pi&ugrave; nell&#39;ambito della vendita per una serie di motivi, uno di questi &egrave; dato dal fatto che il logo utilizzato &egrave; protetto, altri sono da ricercare nelle incoraggianti leggi italiane...&nbsp; :grrr:<br />
	E&#39; il motivo per cui tutto quanto, oneri e onori, rimane in mano all&#39;Associazione senza scopo di lucro francese <em>Borsalinux</em>, chiaramente inserendo la Boutique su FOL aiuteremo in qualche modo a mantenere attivo e ben gestito l&#39;account.</p>
<p>Le richieste in passato sono state parecchie, spero vi piaccia e che trovate qualche gadget che sia di vostro interesse.</p>
<p><a href="http://www.fedoraonline.it/boutique">http://www.fedoraonline.it/boutique</a></p>
