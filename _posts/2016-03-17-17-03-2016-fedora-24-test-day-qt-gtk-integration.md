---
categories:
- staff news
layout: ultime_news
title: '17/03/2016: Fedora 24 Test Day (Qt Gtk Integration)'
created: 1458198874
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p>Il Fedora Test Day di oggi, si focalizzer&agrave; sull&#39;integrazione di applicazioni Qt in Fedora 24 Workstation (Gnome Shell).<br />
	<br />
	REQUISITI PER IL TEST-DAY<br />
	1. Un sistema Fedora 24, completamente aggiornato (tramite dnf update -y)<br />
	2. Pacchetti installati:<br />
	- adwaita-qt4<br />
	- adwaita-qt5<br />
	- qgnomeplatform (richiesta versioneqgnomeplatform-0.1-4.fc24)<br />
	<br />
	3. QGnomePlatform deve avere il parametro QT_QPA_PLATFORMTHEME impostato al valore &#39;qgnomeplatform&#39; (export QT_QPA_PLATFORMTHEME=&#39;qgnomeplatform&#39;). Occorre tenere presente che &quot;export&quot; influenza solo la sessione del terminale da cui viene lanciato, ma il comando pu&ograve; essere reso permanente se aggiunto al proprio file &quot;.bashrc&quot;. In Gnome 3.20, questo passaggio non &egrave; necessario, in quanto tale propriet&agrave; &egrave; gi&agrave; impostata da gnome-session.<br />
	<br />
	<br />
	TEST CASES<br />
	<br />
	Temi:<br />
	1. Assicurarsi di eseguire Fedora Workstation (GNOME) e di aver installato tutti i pacchetti necessar.<br />
	2. Installare una o pi&ugrave; applicazioni Qt4 (come Kmail) e una o pi&ugrave; Qt5 (come Kcalc)<br />
	3. Eseguire le applicazioni e osservarne la grafica<br />
	4. La grafica e l&#39;esperienza fisiva devono essere simili a quelle delle applicazioni GTK con il tema Adwaita. I framework sono diversi, e diversi sono gli elementi dell&#39;interfaccia: per questo, il look non sar&agrave; mai identico, ma deve comunque essere sufficientemente simile<br />
	<br />
	Configurazione dei font:<br />
	1. Assicurarsi di eseguire Fedora Workstation (GNOME) e di aver installato tutti i pacchetti necessari<br />
	2. Installare Gnome Tweak Tool 2, una o pi&ugrave; applicazioni Qt4 (come Kmail) e una o pi&ugrave; Qt5 (come Kcalc)<br />
	3. Operare cambiamenti nelle impostazioni dei font (tipo, dimensione) e contrallare se il carattere cambia effettivamente anche in applicazioni Qt4 e Qt5 (potrebbe essere necessario riavviare tali programmi per applicare i cambiamenti).<br />
	4. Applicazioni Qt4 e Qt5 dovrebbero riflettere i cambiamenti dei font, operati all&#39;interno delle impostazioni GNOME.<br />
	<br />
	Scaling:<br />
	1. Assicurarsi di eseguire Fedora Workstation (GNOME) e di aver installato tutti i pacchetti necessari<br />
	2. Installare GNOME Tweak Tool<br />
	3. Installare una o pi&ugrave; applicazioni Qt4 (come Kmail) e una o pi&ugrave; Qt5 (come Kcalc)<br />
	4. Eseguire GNOME Tweak Tool, navigare alla sezione &quot;Finestre&quot; e impostare il parametro &quot;Scala finestra&quot; a 2.<br />
	5. Anche le finestre delle applicazioni Qt dovrebbero essere scalate di 2.<br />
	<br />
	<br />
	<br />
	RISULTATI<br />
	I risultati vanno inseriti all&#39;interno della pagina dell&#39;evento, nella sezione apposita, in coda al documento.<br />
	Eventuali problemi evidenziati dai tests, devono essere segnalati a Bugzilla, per il componente QGnomePlatform (in caso di problemi di font o scaling) o adwaita-qt (in caso di problemi con il tema adwaita per applicazioni Qt).<br />
	<br />
	LINK UTILI:<br />
	- Pagina ufficiale dell&#39;evento: <a href="https://fedoraproject.org/wiki/Test_Day:2016-03-17_Qt_Gtk_Integration">https://fedoraproject.org/wiki/Test_Day:2016-03-17_Qt_Gtk_Integration</a><br />
	- Panoramica a riguardo dei TestDays: <a href="http://doc.fedoraonline.it/TestDays">http://doc.fedoraonline.it/TestDays</a></p>
