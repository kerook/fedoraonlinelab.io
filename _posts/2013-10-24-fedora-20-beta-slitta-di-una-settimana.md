---
categories:
- staff news
layout: ultime_news
title: Fedora 20 Beta slitta di una settimana
created: 1382646087
---
<p>Ad oggi non abbiamo nemmeno una Release Candidate, e con questo abbiamo detto gi&agrave; molto.</p>
<p>Fedora 20 Beta slitta almeno di una settimana per svariati bug bloccanti, i pi&ugrave; di Ananconda, ma anche il DVD per adesso non pu&ograve; essere compilato perch&egrave; oversized. Tutti i blocker si possono leggere qui:</p>
<p><a href="https://qa.fedoraproject.org/blockerbugs/milestone/20/beta/buglist">https://qa.fedoraproject.org/blockerbugs/milestone/20/beta/buglist</a></p>
<p>La nuova tabella di marcia verso il rilascio della Fedora 20 finale, prevista per il 10 dicembre, &egrave; consultabile qui:</p>
<p><a href="https://fedoraproject.org/wiki/Releases/20/Schedule">https://fedoraproject.org/wiki/Releases/20/Schedule</a></p>
<p>&nbsp;</p>
