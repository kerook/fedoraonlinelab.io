---
categories:
- staff news
layout: ultime_news
title: Seth, dedicata a te! Rilasciata Fedora 20
created: 1387312826
---
<p>Heisenbug, la tanto attesa ventesima versione di Fedora, che coincide con il decimo anniversario di Fedora, &egrave; stata rilasciata poche ore fa.</p>
<p>Fedora 20 &egrave; interamente dedicata a Seth Vidal, scomparso lo scorso 8 luglio, che per anni ha contribuito allo sviluppo di Fedora e della sua infrastruttura. E&#39; anche il padre di Yum, il gestore pacchetti che tutti utilizziamo quotidianamente.</p>
<p>Le principali novit&agrave; sono:</p>
<ul>
	<li>
		Gnome 3.10</li>
	<li>
		KDE 4.11</li>
	<li>
		ARM &egrave; considerata architettura primaria</li>
	<li>
		Miglioramenti Cloud e Virtualizzazione</li>
	<li>
		Ruby 4.0</li>
</ul>
<p>e molto altro ancora.</p>
<p>Prima di installare Fedora 20 &egrave; bene dare un&#39;occhiata ai bug pi&ugrave; comuni: <a href="http://fedoraproject.org/wiki/Common_F20_bugs">http://fedoraproject.org/wiki/Common_F20_bugs</a></p>
<p>Le Note di Rilascio sono disponibili su <a href="http://docs.fedoraproject.org/en-US/Fedora/20/html/Release_Notes/index.html">http://docs.fedoraproject.org/en-US/Fedora/20/html/Release_Notes/index.html</a> mentre&nbsp; l&#39;annuncio ufficiale &egrave; reperibile qui: <a href="https://lists.fedoraproject.org/pipermail/devel-announce/2013-December/001282.html">https://lists.fedoraproject.org/pipermail/devel-announce/2013-December/001282.html</a></p>
<p>Buona Fedora 20 e buon Natale a tutti!</p>
