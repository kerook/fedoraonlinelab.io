---
categories:
- staff news
layout: ultime_news
title: '16/05/2016: Fedora 24 Test Day (Workstation Graphical Upgrade)'
created: 1463161527
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p>&nbsp;</p>
<p><strong>Introduzione</strong></p>
<p>Nella giornata di Luned&igrave; 16 Maggio 2016, &egrave; prevista una giornata di testing.<br />
	<br />
	L&#39;obiettivo sar&agrave; quello di sperimentare l&#39;upgrade da Fedora 23 a Fedora 24 Workstation, tramite Gnome-Software<br />
	<br />
	La pagina relativa all&#39;evento &egrave; la seguente: <a href="https://fedoraproject.org/wiki/Test_Day:2016-05-16_Workstation_Graphical_Upgrade">https://fedoraproject.org/wiki/Test_Day:2016-05-16_Workstation_Graphical_Upgrade</a><br />
	I risultati devono essere inseriti separatamente: <a href="http://testdays.fedorainfracloud.org/events/8">http://testdays.fedorainfracloud.org/events/8</a><br />
	<br />
	&nbsp;<br />
	<strong>Preparazione del sistema</strong>:<br />
	<br />
	- Sistema Fedora 23 (macchina virtuale o hardware fisico) completamente aggiornato. Si raccomanda di non utilizzare installazioni che racchiudono, al loro interno, dati importanti, per evitare ogni possibile rischio.</p>
<p><br />
	<br />
	<strong>Come testare</strong>:<br />
	<br />
	- Effettuare il boot delle immagini - reperibili nella pagina principale dell&#39;evento - all&#39;interno di un sistema cloud, su Vagrant o usando testcloud.</p>
<p>- Successivamente, occorre eseguire i test elencati nella pagina dei risultati</p>
<p><br />
	<strong>&nbsp;&nbsp; &nbsp;<br />
	Casi da testare</strong>:<br />
	<br />
	- &Egrave; stato preparato un <a href="https://fedoraproject.org/wiki/QA:Testcase_Workstation_Graphical_Upgrade">test case</a> apposito.</p>
<p>- Eventuali segnalazioni devono essere aperte nei confronti del componente &quot;<code>gnome-software</code>gnome-software&quot; e devono essere configurate come blocker del bug <span class="extiw"><a href="https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=1308538">1308538</a>.</span><br />
	&nbsp;<br />
	<br />
	<strong>IRC</strong>:<br />
	<br />
	Il canale internazionale ufficiale per l&#39;evento &egrave; il consueto #fedora-test-day.</p>
<p>&nbsp;</p>
<p><strong>Link utili documentazione FedoraOnline</strong>:</p>
<p>- <a href="http://doc.fedoraonline.it/TestDays">TestDays</a></p>
<p>- <a href="http://doc.fedoraonline.it/Segnalare_un_bug">Come segnalare un bug</a></p>
