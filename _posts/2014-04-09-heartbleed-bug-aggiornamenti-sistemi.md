---
categories:
- staff news
layout: ultime_news
title: '"Heartbleed" bug: aggiornamenti sistemi!'
created: 1397044806
---
<p>Da venerd&igrave; scorso la parola &quot;heartbleed&quot; circola nel web, nei social networks, su riviste ecc... Si tratta di una delle pi&ugrave; grandi falle scoperte di OpenSSL. OpenSSL, lo ricordiamo, &egrave; un servizio utilizzato in tutto il mondo per le connessioni sicure e viene utilizzato per tutte le operazioni in cui serve una sicurezza di altissimo livello. Transazioni, password, connessioni a server con dati sensibili ecc. ecc.</p>
<p>&quot;Heartbleed&quot; sfrutta l&#39;abitudine di OpenSSL di verificare costantemente se l&#39;utente dall&#39;altra parte c&#39;&egrave; ancora o se &egrave; andato via, inviando una specie di ping alla macchina connessa. In caso di non risposta il servizio chiude la connessione automaticamente.</p>
<p>Il bug scoperto azzera totalmente la sicurezza delle connessioni con OpenSSL, anzi, ha l&#39;effetto contrario,motivo per cui tutti i sistemi, server, e siti web stanno in queste ore applicando la patch necessaria (e resa subito disponibile) per fixare il problema. Il problema &egrave; che attualmente nessuno riesce a capire se un determinato sito o collegamento ha gi&agrave; provveduto a questo fix oppure no, per cui si consiglia di dare particolare attenzione quando si fanno delle transazioni o altre operazioni delicate. Naturalmente il primo anello debole &egrave; sempre l&#39;utente stesso, per cui &egrave; <span style="color:#ff0000;"><u><strong>NECESSARIO&nbsp;</strong></u></span>procedere con l&#39;aggiornamento dei pacchetti e librerie di OpenSSL sulla propria macchina:</p>
<p>#&nbsp;<span style="font-family: arial, sans-serif; font-size: 13px;">yum update openssl openssl-libs</span></p>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">E&#39; necessario riavviare i servizi OpenSSL e tutti gli altri demoni connessi ad esso in qualche modo. Se non si tratta di una macchina che deve rimanere accesa, si pu&ograve; semplicemente riavviare il PC dopo l&#39;aggiornamento.</span></p>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">Gi&agrave; nella giornata di ieri le immagini Cloud di Fedora 20 sono state rimpiazzate con nuove immagini compilate includendo la patch. Le altre immagini dovrebbero essere aggiornate nelle prossime ore, anche se , una volta che si scarica una ISO di Fedora 20 e si esegue un aggiornamento completo del sistema, il problema non esiste pi&ugrave;.</span></p>
<p><span style="font-family: arial, sans-serif; font-size: 13px;">L&#39;annuncio del Project Leader di Fedora lo potete leggere qui:</span></p>
<p><a href="https://lists.fedoraproject.org/pipermail/announce/2014-April/003207.html">https://lists.fedoraproject.org/pipermail/announce/2014-April/003207.html</a></p>
