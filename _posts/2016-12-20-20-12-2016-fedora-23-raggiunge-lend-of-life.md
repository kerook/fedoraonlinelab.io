---
categories:
- staff news
layout: ultime_news
title: '20/12/2016: Fedora 23 raggiunge l''End Of Life!'
created: 1482216772
---
<p><img alt="" src="https://cdn.fedoramagazine.org/wp-content/uploads/2016/12/fedora23eol-945x400.jpg" style="width: 399px; height: 169px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">Nel corso della giornata del <strong>20 Dicembre 2016</strong>, il rilascio 23 di Fedora raggiunger&agrave; lo stato di <strong>End Of Life</strong>.</p>
<p class="rtejustify">Questo implica che, alla data indicata:</p>
<ul>
	<li class="rtejustify">
		I pacchetti nei repository di Fedora 23, non riceveranno pi&ugrave; aggiornamenti (neanche quelli pi&ugrave; importanti, relativi alla sicurezza).</li>
	<li class="rtejustify">
		Nessun nuovo software verr&agrave; aggiunto ai repository di Fedora 23.</li>
	<li class="rtejustify">
		&Egrave; consigliato l&#39;upgrade verso una versione attivamente mantenuta (Fedora 24 o Fedora 25).</li>
</ul>
<p class="rtejustify">Una <a href="http://www.fedoraonline.it/content/221116-oggi-%C3%A8-il-giorno-di-fedora-25">notizia recente</a>, riepiloga le novit&agrave; di Fedora 25, resa disponibile il giorno 22 Novembre 2016. Gli utenti che preferiscono un sistema operativo pi&ugrave; maturo, possono optare per Fedora 24, che sar&agrave; mantenuto fino al mese successivo al rilascio finale di Fedora 26 (<a href="https://fedoraproject.org/wiki/Releases/26/Schedule">la cui uscita &egrave; programmata per Giugno 2017</a>)</p>
<p class="rtejustify">A tal proposito, Fedora Online mette a disposizione alcune guide:</p>
<ul>
	<li class="rtejustify">
		Installazione pulita di Fedora 25 (<a href="http://doc.fedoraonline.it/Installazione_Fedora_22">http://doc.fedoraonline.it/Installazione_Fedora_22</a>) (<a href="http://doc.fedoraonline.it/Installazione_Fedora">http://doc.fedoraonline.it/Installazione_Fedora</a>).</li>
	<li class="rtejustify">
		Avanzamento di versione da Fedora 23 a Fedora 24 o 25 (<a href="http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade">http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade</a>).</li>
</ul>
