---
categories:
- staff news
layout: ultime_news
title: Fedora 24 Alpha slitta di una settimana
created: 1458240041
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">Si &egrave; da poco conclusa la riunione IRC volta a decidere in merito all&#39;eventuale rilascio della milestone Alpha di Fedora 24. Il log &egrave; reperibile al seguente link: <a href="https://meetbot.fedoraproject.org/fedora-meeting/2016-03-17/f24-alpha-go_no_go-meeting.2016-03-17-17.00.log.html">https://meetbot.fedoraproject.org/fedora-meeting/2016-03-17/f24-alpha-go_no_go-meeting.2016-03-17-17.00.log.html</a>.</p>
<p class="rtejustify">Il responso &egrave; stato negativo, a causa del seguente bug bloccante, ancora non del tutto risolto:</p>
<p class="rtejustify">-<em><span id="summary_alias_container"><span id="short_desc_nonedit_display">cockpit.socket not enabled automatically on Fedora Server installs</span></span></em> (<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1317721">https://bugzilla.redhat.com/show_bug.cgi?id=1317721</a>)</p>
<p>&nbsp;</p>
<p>Appuntamento a mercoled&igrave; 23 Marzo 2016 (ore 18:00 italiane) per un altro Go/No-Go Meeting.</p>
<p>La nuova data per il rilascio &egrave; invece prevista per il <strong><u>29 Marzo 2016</u></strong> (ovvviamente subordinata alla decisione finale dell&#39;incontro del 23).</p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">Link utili:<br />
	- Informazioni relative ai Go/No-go Meeting <a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">https://fedoraproject.org/wiki/Go_No_Go_Meeting</a><br />
	- Criteri di rilascio per Fedora 24 Alpha: <a href="https://fedoraproject.org/wiki/Fedora_24_Alpha_Release_Criteria">https://fedoraproject.org/wiki/Fedora_24_Alpha_Release_Criteria</a><br />
	- Lista aggiornata dei bug bloccanti: <a href="http://qa.fedoraproject.org/blockerbugs/milestone/24/alpha/buglist">http://qa.fedoraproject.org/blockerbugs/milestone/24/alpha/buglist</a><br />
	- Fedora 24 release schedule: <a href="https://fedoraproject.org/wiki/Releases/24/Schedule">https://fedoraproject.org/wiki/Releases/24/Schedule</a></p>
<p><a a="" href="https://fedoraproject.org/wiki/Releases/24/Schedule"> </a></p>
<p class="rtejustify">&nbsp;</p>
