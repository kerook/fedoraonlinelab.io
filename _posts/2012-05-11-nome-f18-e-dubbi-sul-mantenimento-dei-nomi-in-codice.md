---
categories:
- staff news
layout: ultime_news
title: Nome F18 e dubbi sul mantenimento dei nomi in codice
created: 1336725257
---
<p>
	Nelle ultime settimane &egrave; stato scelto il nome di Fedora 18, che sar&agrave; identificata come <span style="font-weight:bold">Spherical Cow</span>. L&#39;annuncio ufficiale con i rispettivi voti pu&ograve; essere consultato qui:</p>
<p>
	<a href="http://lists.fedoraproject.org/pipermail/announce/2012-April/003067.html">http://lists.fedoraproject.org/pipermail/announce/2012-April/003067.html</a></p>
<p>
	La difficolt&agrave; nel trovare un nome per la diciottesima release, viste anche le caratteristiche che devono avere i nuovi nomi, ha spinto la comunit&agrave; a mettere in dubbio se mantenere o meno questa usanza oppure, come proposto da molti, di chiamare le versioni di Fedora semplicemente Fedora 19, 20, 21, ecc...</p>
<p>
	Il risultato del sondaggio lanciato &egrave; stato quello di mantenere comunque i nomi in codice, ma &egrave; stato deciso che in futuro verr&agrave; modificato il modo in cui verranno proposti i nuovi nomi, e che secondo la nostra opinione &egrave; anche la strada pi&ugrave; sensata. I risultati sono consultabili qui:</p>
<p>
	https://admin.fedoraproject.org/voting/results/poll-rel-names?_csrf_token=ed7209dc06f5c8c58528ac55251f52c8ffacdc40</p>
