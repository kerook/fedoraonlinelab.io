---
categories:
- staff news
layout: ultime_news
title: A partire da Fedora 23 FedUp verrà dismesso. Arriva dnf-plugin-system-upgrade
created: 1442611563
---
<p>Come annunciato da tempo, a causa di alcune incompatibilit&agrave; con systemd, per gli aggiornamenti da Fedora 22 a Fedora 23 (e seguenti) FedUp verr&agrave; <span id="result_box" lang="it"><span title="For upgrades to F23, fedup is being replaced with dnf-plugin-system-upgrade, a DNF plugin that handles system upgrades using systemd's Offline System Updates.

">stato sostituito con DNF-plugin-system-upgrade, un plugin DNF che gestisce gli aggiornamenti di sistema utilizzando l&#39;Offline System Updates di systemd.</span></span></p>
<p><span lang="it"><span title="For upgrades to F23, fedup is being replaced with dnf-plugin-system-upgrade, a DNF plugin that handles system upgrades using systemd's Offline System Updates.

">I benefici per il sistema Fedora possono essere cos&igrave; riassunti:</span></span></p>
<ul>
	<li>
		<span lang="it"><span title="For upgrades to F23, fedup is being replaced with dnf-plugin-system-upgrade, a DNF plugin that handles system upgrades using systemd's Offline System Updates.

">Un p</span></span><span id="result_box" lang="it"><span title="The upgrade process will be much more reliable, and well-integrated with the system packaging tools.

">rocesso di aggiornamento molto pi&ugrave; affidabile e ben integrato con gli strumenti di packaging del sistema</span></span>
		<ul>
			<li>
				<span id="result_box" lang="it"><span title="Upgrades support distro-sync mode
&nbsp;&nbsp;&nbsp;&nbsp;">Aggiornamenti al supporto modalit&agrave; distro-sync</span></span></li>
			<li>
				<span id="result_box" lang="it"><span title="Systems do not need to (re-)download update metadata after the upgrade

">I sistemi non hanno bisogno di (ri)scaricare l&#39;aggiornamento dei metadati dopo l&#39;aggiornamento</span></span></li>
		</ul>
	</li>
	<li>
		<span id="result_box" lang="it"><span title="We no longer need to build upgrade.img, which saves space in boot/install media.

">Non c&#39;&egrave; pi&ugrave; bisogno di costruire upgrade.img, consentendo di risparmiare spazio nel supporto di installazione/boot</span></span></li>
</ul>
<p>&nbsp;</p>
<p>Di seguito l&#39;annuncio ufficiale di Will Woods:</p>
<p>https://lists.fedoraproject.org/pipermail/devel/2015-May/210905.html</p>
<p>Per chi volesse testare il nuovo sistema di aggiornamento pu&ograve; seguire le istruzioni al seguente topic di FOL aperto dall&#39;ottimo arkanoid e prestando particolare attenzione ai bug ancora aperti:</p>
<p>http://forum.fedoraonline.it/viewtopic.php?id=23810</p>
<p>&nbsp;</p>
<p>Buon aggiornamento a tutti!</p>
<p>&nbsp;</p>
