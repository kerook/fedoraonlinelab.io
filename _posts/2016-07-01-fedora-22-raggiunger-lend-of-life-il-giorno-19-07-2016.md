---
categories:
- staff news
layout: ultime_news
title: Fedora 22 raggiungerà l'End Of Life il giorno 19/07/2016
created: 1467401167
---
<p><img alt="" src="https://cdn.fedoramagazine.org/wp-content/uploads/2016/06/fedora22eol-945x400.png" style="width: 399px; height: 169px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">Il giorno <strong>19 Luglio 2016</strong>, il rilascio 22 di Fedora, raggiunger&agrave; l&#39;<strong>End Of Life</strong>.</p>
<p class="rtejustify">Questo implica che, alla data indicata:</p>
<ul>
	<li class="rtejustify">
		I pacchetti nei repository di Fedora 22, non riceveranno pi&ugrave; aggiornamenti (neanche quelli pi&ugrave; importanti, relativi alla sicurezza).</li>
	<li class="rtejustify">
		Nessun nuovo software verr&agrave; aggiunto ai repository di Fedora 22.</li>
	<li class="rtejustify">
		&Egrave; consigliato effettuare un upgrade verso una versione attivamente mantenuta (Fedora 23 o Fedora 24).</li>
</ul>
<p class="rtejustify">Una <a href="http://www.fedoraonline.it/content/rilasciata-fedora-24">notizia recente</a>, riepiloga le novit&agrave; di Fedora 24, resa disponibile il giorno 26 Giugno 2016. Gli utenti che preferiscono un sistema operativo pi&ugrave; maturo, possono optare per Fedora 23, che sar&agrave; mantenuto fino al mese successivo al rilascio finale di Fedora 25 (<a href="https://fedoraproject.org/wiki/Releases/25/Schedule">la cui uscita &egrave; programmata per Novembre</a>)</p>
<p class="rtejustify">A tal proposito, Fedora Online mette a disposizione alcune guide:</p>
<ul>
	<li class="rtejustify">
		Installazione pulita di Fedora 24 (<a href="http://doc.fedoraonline.it/Installazione_Fedora_22">http://doc.fedoraonline.it/Installazione_Fedora_22</a>) (<a href="http://doc.fedoraonline.it/Installazione_Fedora">http://doc.fedoraonline.it/Installazione_Fedora</a>).</li>
	<li class="rtejustify">
		Avanzamento di versione da Fedora 22 a Fedora 23 o 24 (<a href="http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade">http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade</a>).</li>
</ul>
