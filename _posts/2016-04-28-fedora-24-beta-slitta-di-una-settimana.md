---
categories:
- staff news
layout: ultime_news
title: Fedora 24 Beta slitta di una settimana
created: 1461876489
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p>Si &egrave; oggi tenuta la riunione IRC volta a decidere in merito all&#39;eventuale rilascio della milestone Beta di Fedora 24. Il log &egrave; reperibile <a href="https://meetbot.fedoraproject.org/fedora-meeting/2016-04-28/f24-beta-go_no_go-meeting.2016-04-28-17.00.log.html">qui</a>.<br />
	<br />
	Il responso &egrave; stato negativo, a causa sopratutto del seguente bug bloccante. Esso &egrave; stato gi&agrave; risolto, ma non in tempo per assicurare un sufficiente livello qualitativo (in termini di testing) ed evitare lo slittamento:<br />
	<br />
	- <em>Live images in Fedora 24 Beta-1.1 compose are labelled &#39;Alpha&#39; (<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1331504">https://bugzilla.redhat.com/show_bug.cgi?id=1331504</a>)</em><br />
	&nbsp;</p>
<p>&nbsp;Appuntamento a Gioved&igrave; 05 Maggio 2016 (ore 19:30 italiane) per un altro Go/No-Go Meeting.<br />
	&nbsp;La nuova data per il rilascio &egrave; invece prevista per il <u><strong>10 Maggio 2016</strong></u> (ovvviamente subordinata alla decisione finale dell&#39;incontro del 05).</p>
<p><br />
	Link utili:<br />
	- Informazioni relative ai Go/No-go Meeting <a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">https://fedoraproject.org/wiki/Go_No_Go_Meeting</a><br />
	- Criteri di rilascio per Fedora 24 Beta: <a href="https://fedoraproject.org/wiki/Fedora_24_Beta_Release_Criteria">https://fedoraproject.org/wiki/Fedora_24_Beta_Release_Criteria</a><br />
	- Lista aggiornata dei bug bloccanti: <a href="http://qa.fedoraproject.org/blockerbugs/milestone/24/beta/buglist">http://qa.fedoraproject.org/blockerbugs/milestone/24/beta/buglist</a><br />
	- Fedora 24 release schedule: <a href="https://fedoraproject.org/wiki/Releases/24/Schedule">https://fedoraproject.org/wiki/Releases/24/Schedule</a></p>
<p><a a="" href="https://fedoraproject.org/wiki/Releases/24/Schedule"> </a></p>
<p class="rtejustify">&nbsp;</p>
