---
categories:
- staff news
layout: ultime_news
title: Rilasciato Gnome 3.22!
created: 1474573882
---
<p class="rtejustify"><img alt="" src="https://i2.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2015/09/gnome3-18.png?w=945&amp;ssl=1" style="width: 400px; height: 169px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">Il presente articolo, &egrave; un adattamento in italiano dell&#39;<a href="https://fedoramagazine.org/gnome-3-22-released/">originale</a> scritto da Ryan Lerch per Fedora Magazine.<br />
	<br />
	La GNOME Community ha da poco <a href="https://www.gnome.org/news/2016/09/gnome-3-22-released-the-future-is-now/">annunciato</a> il rilascio ufficiale di GNOME 3.22. Quello che con ogni probabilit&agrave; sar&agrave; l&#39;ambiente desktop di Fedora Workstation 25, fornisce una moltitudine di novit&agrave;, tra cui una versione aggiornata di programmi quali <a href="https://fedoramagazine.org/batch-file-renaming-integrated-archive-support-added-nautilus/">Files</a> e Software (quest&#39;ultimo fornir&agrave; supporto ad applicazioni <a href="https://fedoramagazine.org/introducing-flatpak/">Flatpak</a> al suo interno).<br />
	<br />
	Gli utenti Fedora desiderosi di provare le nuove caratteristiche dell&#39;ambiente, possono installare <a href="https://getfedora.org/en/workstation/prerelease/">Fedora 25</a>, che attualmente contiene una versione di testing. Il sistema operativo in questione, ricever&agrave; presto aggiornamenti per includere il rilascio stabile di GNOME 3.22. Come alternativa, &egrave; possibile installare individualmente alcune applicazioni via <a href="http://flatpak.org/apps.html">Flatpak</a>.</p>
<p class="rtejustify"><br />
	<br />
	<strong>Files (nautilus)</strong><br />
	<br />
	&quot;Files (nautilus)&quot; &egrave; una delle applicazioni dell&#39;ecosistema GNOME che ha ricevuto pi&ugrave; aggiornamenti per il rilascio 3.22. Come gi&agrave; segnalato su <a href="https://fedoramagazine.org/batch-file-renaming-integrated-archive-support-added-nautilus/">Fedora Magazine</a>, essa possiede ora una nuova ed elegante funzione di rinominazione batch di file.</p>
<p class="rtecenter"><a href="https://i1.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/files-batchrenaming.png?resize=768%2C406&amp;ssl=1"><img alt="" src="https://i1.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/files-batchrenaming.png?resize=768%2C406&amp;ssl=1" style="width: 378px; height: 200px;" /></a></p>
<p class="rtejustify"><br />
	<br />
	Ulteriori novit&agrave; riguardano l&#39;aggiornamento dei controlli per le opzioni di ordinamento e visualizzazione, che consentono ora di passare dalla &quot;modalit&agrave; griglia&quot; alla &quot;modalit&agrave; lista&quot;. Il meccanismo di zoom &egrave; stato inoltre semplificato. Questi cambiamenti sono stati implementati grazie alle indicazioni ricavate dalle sessioni di testing sull&#39;usabilit&agrave;, promosse da Gina Dobrescu (<a href="https://www.gnome.org/outreachy/">Outrachy</a> intern).</p>
<p class="rtecenter"><a href="https://i0.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/files.png?ssl=1"><img alt="" src="https://i0.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/files.png?ssl=1" style="width: 427px; height: 200px;" /></a></p>
<p class="rtejustify"><br />
	<br />
	&nbsp;</p>
<p class="rtejustify"><strong>Software</strong></p>
<p class="rtecenter"><strong><a href="https://i2.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/software.png?resize=768%2C400&amp;ssl=1"><img alt="" src="https://i2.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/software.png?resize=768%2C400&amp;ssl=1" style="width: 384px; height: 200px;" /></a></strong></p>
<p class="rtejustify"><br />
	Anche il gestore &quot;Software&quot; &egrave; stato aggiornato: il men&ugrave; principale contiene ora pi&ugrave; icone ed applicazioni rispetto al passato. I voti sottoforma di stelle - che erano stati introdotti da un rilascio precedente - sono ora meglio evidenziati e nuovi simboli colorati indicato se un pacchetto contiene Free Software. Come gi&agrave; detto, &egrave; ora supportata l&#39;installazione di applicazioni Flatpak, provenienti dagli appositi repository.<br />
	<br />
	&nbsp;</p>
<p class="rtejustify"><strong>Impostazioni della tastiera</strong></p>
<p class="rtecenter"><strong><a href="https://i1.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/keyboard-settings.png?resize=768%2C457&amp;ssl=1"><img alt="" src="https://i1.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/keyboard-settings.png?resize=768%2C457&amp;ssl=1" style="width: 336px; height: 200px;" /></a></strong></p>
<p class="rtejustify"><br />
	Con GNOME 322, anche le impostazioni della tastiera sono state aggiornate. Esse forniscono ora metodi pi&ugrave; facili per cercare e configurare le scorciatoie e le opzioni della periferica di input.<br />
	<br />
	&nbsp;</p>
<p class="rtejustify"><strong>Ulteriori informazioni</strong><br />
	<br />
	Per avere ulteriori informazioni sulla composizione del rilascio 3.22, si consiglia di consultare l&#39;<a href="https://www.gnome.org/news/2016/09/gnome-3-22-released-the-future-is-now/">annuncio ufficiale</a> e le <a href="https://help.gnome.org/misc/release-notes/3.22/">note di rilascio</a>.</p>
