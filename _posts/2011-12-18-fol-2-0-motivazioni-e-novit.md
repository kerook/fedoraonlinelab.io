---
categories:
- staff news
layout: ultime_news
title: 'Fol 2.0: motivazioni e novità'
created: 1324215647
---
<p>
	Per spiegare tutte le novit&agrave; della versione 2.0 di Fedora Online ci vorrebbero svariate pagine. Cercher&ograve; di spiegare le motivazioni delle scelte e le novit&agrave;, in modo da dare un piccolo aiuto a chi si trova spaesato sulla nuova piattaforma. La storia di Fol e le sue versioni sono state inserite nelle pagine apposite nel footer, per chi non le avesse ancora viste.</p>
<p>
	<strong>Premessa</strong></p>
<p>
	L&#39;esigenza di abbandonare la vecchia versione &egrave; nata dopo alcune valutazioni fatte insieme allo staff e dalla richiesta di alcuni utenti. In sintesi le problematiche erano:</p>
<ul>
	<li>
		Xoops non era pi&ugrave; in grado di supportare il forum, inoltre non erano previsti rilasci core in xhtml strict;</li>
	<li>
		Il database sul server era ormai tenuto su con le toppe, era diventato troppo grande e ci creava problemi con il server stesso;</li>
	<li>
		le guide di Fol erano diventate inconsultabili perch&egrave; introvabili;</li>
	<li>
		Volevamo abbandonare il transitional e andare su xhtml strict.</li>
</ul>
<p>
	Inoltre, essendo anche il destinatario dell&#39;indirizzo della comunit&agrave; di Fedora in Italia http://it.fedoracommunity.org volevamo adeguarci allo stile di Fedora, abbandonando il colore arancione che ci aveva contraddistinto in tutti questi anni.</p>
<p>
	Dopo alcune considerazioni la scelta degli applicativi da usare era abbastanza chiara, la scelta &egrave; caduta su:</p>
<ul>
	<li>
		Fluxbb perch&egrave; riesce a gestire forum molto grandi con velocit&agrave; di risposta discrete; inoltre esistono svariati modi di personalizzazione e la struttura &egrave; facilmente da modificare;</li>
	<li>
		Mediawiki per la gestione della documentazione, permette molte personalizzazioni ed &egrave; forse il miglior software per un wiki;</li>
	<li>
		Drupal perch&egrave; permette fin da subito un codice xhtml strict, e sebbene non sia facile da capire all&#39;inizio ha un&#39;ottima documentazione di sviluppo, in poco tempo si possono sviluppare dei moduli aggiuntivi, &egrave; facilmente estendibile/configurabile con i moduli e ha una buona stabilit&agrave;.</li>
</ul>
<p>
	Ultimo ma non ultimo era necessario riuscire a sviluppare dei plugin ad hoc per far s&igrave; che le sezioni si &quot;parlassero&quot; tra di loro.</p>
<p>
	Mi sembra doveroso ringraziare l&#39;amico <a href="http://fedoraproject.org/wiki/User:Llaumgui">llaumgui</a>, webmaster della comunit&agrave; francese&nbsp;<a href="http://fedora-fr.org">http://fedora-fr.org,</a> per qualche dritta iniziale, tanto che non avremmo nulla contro un possibile gemellaggio. Anche se non utilizzano gli stessi software e hanno sviluppato plugin diversi, al momento dello sviluppo della struttura di Fol 2.0 (ricordo ancora i messaggi in piena estate) ci ha aiutato. Fedora vuol dire anche questo!</p>
<p>
	Ma veniamo a noi e alle novit&agrave; pratiche:</p>
<ul>
	<li>
		Inanzitutto &egrave; importante dirvi che abbiamo abolito tutte le cose che non aiutavano alla crescita di una comunit&agrave; come la nostra. Non &egrave; importante se uno ha pi&ugrave; o meno post, non importano livelli e stelline, contano invece i contributi che si danno;</li>
	<li>
		Il wiki &egrave; la prima dimostrazione di questo primo aspetto: i &quot;redattori&quot;, che potranno diventarlo come scritto <a href="http://doc.fedoraonline.it/Contribuisci">qui</a>, scriveranno le guide in collaborazione con lo staff e avranno la paternit&agrave; come autori. Il wiki in questo modo non solo avr&agrave; articoli qualitativamente migliori, ma in questo modo le guide saranno sempre aggiornate (perch&egrave; mantenute costantemente);</li>
	<li>
		Ogni utente potr&agrave; caricare fin da subito il proprio avatar in formato 80x80, senza dover aspettare un numero minimo di post;</li>
	<li>
		Abbiamo migliorato il processo di registrazione per evitare account creati da macchine o a fini spam;</li>
	<li>
		Abbiamo migliorato il contrasto, spesso contestato su Fol 1.0, e distinto maggiormente il codice dalle citazioni;</li>
	<li>
		Esistono dei link nel forum personalizzati per l&#39;utente, nel senso che si possono vedere le discussioni proprie oppure quelle che ci sono state dall&#39;ultima visita del singolo utente; inoltre nelle opzioni il forum &egrave; ulteriormente personalizzabile come impostazioni di visualizzazione;</li>
	<li>
		La homepage ha volutamente dei link specifici al fedoraproject, come il download o il progetto Fedora; inoltre abbiamo mantenuto le descrizioni di alcuni aspetti fondamentali del fedoraproject in un menu dedicato;</li>
	<li>
		Sono stati reintrodotti gli ultimi aggiornamenti stabili delle release attuali, in questo momento si potranno consultare quelli per Fedora 15 e Fedora 16;</li>
	<li>
		Folio, il magazine di Fedora Online, uscir&agrave; a breve e vogliamo anche qui instaurare un meccanismo di collaborazione tra utenti e redazione per la sua realizzazione;</li>
	<li>
		Tutti i link delle discussioni vecchie di Fol 1 rimandano alle discussioni nel forum nuovo, idem per le guide (per le quali per&ograve; si finisce nella pagina principale del wiki);</li>
	<li>
		Tutto il lavoro che trovate sul sito &egrave; rilasciato con licenza Creative Commons;</li>
	<li>
		E&#39; possibile inviare, se registrati, i propri desktop per far vedere a tutti le personalizzazioni fatte, le immagini sono gestite in overlay;</li>
	<li>
		Il wiki &egrave;, ad eccezione dei redattori, di sola consultazione;</li>
</ul>
<p>
	Non ricordo nemmeno tutto, ma siccome piano piano miglioreremo ancora alcuni aspetti, non li potrei elencare comunque tutti.</p>
<p>
	Una cosa &egrave; per&ograve; fondamentale: La nuova versione di Fedora Online &egrave; pensata per una risoluzione minima di 1024px di larghezza e abbiamo escluso da una visualizzazione corretta Internet Explorer 6 e inferiori. Invece pensiamo di rilasciare in futuro un layout ottimizzato per gli smartphone.</p>
<p>
	Vi ringrazio per i tanti commenti positivi e vi auguro una buona permanenza.</p>
