---
categories:
- staff news
layout: ultime_news
title: 'Fedora 23: Wallpaper aggiuntivi!'
created: 1442907140
---
<p><img alt="" src="http://fedoramagazine.org/wp-content/uploads/2015/07/f23-supp-wp-945x400.png" style="width: 500px; height: 212px;" /></p>
<p>Si &egrave; concluso il consueto contest del fedoraproject per il package dei wallpaper supplementari nel quale ogni utente in possesso di un Fedora account (FAS) pu&ograve; partecipare inviando una propria foto e/o votare per le proprie preferite.<br />
	<br />
	In quest&#39;ultima edizione sono state inviate 199 foto di cui soltanto 157 ritenute idonee al voto, e di queste le prime 16 entreranno di diritto nel pacchetto background-extra di Fedora 23.<br />
	<br />
	A questo link la classifica finale con le relative foto:<br />
	<br />
	https://apps.fedoraproject.org/nuancier/results/4/<br />
	<br />
	Enjoy!</p>
