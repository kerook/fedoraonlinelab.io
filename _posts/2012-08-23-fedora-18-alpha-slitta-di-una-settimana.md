---
categories:
- staff news
layout: ultime_news
title: Fedora 18 Alpha slitta di una settimana
created: 1345724637
---
<p>
	Come riportato sulla ML fedora announce da Jaroslav:</p>
<p>
	http://lists.fedoraproject.org/pipermail/devel-announce/2012-August/000965.html</p>
<p>
	Al Go/No-Go meeting &egrave; stato deciso di far slittare di una settimana il rilascio di Fedora 18 Alpha a causa di numerosi bug inrisolti [1] e test non completi [2][3][4]. Per maggiori dettagli si pu&ograve; fare riferimento al log del meeting [5].</p>
<p>
	Come conseguenza anche la Beta e la versione finale vengono posticipati di una settimana.<br />
	<br />
	All&#39;annuncio dello sviluppatore sono stati segnalati i prossimi meeting importanti per il futuro di Fedora 18 ed &egrave; stata aggiornata la tabella di marcia [6].<br />
	<br />
	[1] http://qa.fedoraproject.org/blockerbugs/current<br />
	[2] https://fedoraproject.org/wiki/Test_Results:Current_Base_Test<br />
	[3] http://fedoraproject.org/wiki/Test_Results:Current_Installation_Test<br />
	[4] http://fedoraproject.org/wiki/Test_Results:Current_Desktop_Test<br />
	[5] http://tinyurl.com/d5pzel9<br />
	[6] http://jreznik.fedorapeople.org/schedules/f-18/</p>
