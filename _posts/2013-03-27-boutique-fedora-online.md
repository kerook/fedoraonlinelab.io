---
categories: []
layout: page
title: Boutique Fedora Online
created: 1364398586
---
<p class="rtecenter"><strong><span style="font-size: 18px"><span style="font-family: verdana, geneva, sans-serif"><span style="color: #3c6eb4">Boutique Fedora Online</span></span></span></strong></p>
<p class="rtecenter">&nbsp;</p>
<p><span style="font-family: verdana, geneva, sans-serif"><span style="font-size: 14px">La boutique di Fedora Online &egrave; stata realizzata in collaborazione con la comunit&agrave; francese <a href="http://fedora-fr.org/">http://fedora-fr.org/</a>&nbsp;alla quale vanno i nostri ringraziamenti per la disponibilit&agrave;. Obiettivo del negozio &egrave; quello di fornire un ulteriore servizio agli utenti Fedora italiani e non avviare un&#39;attivit&agrave; a scopo di lucro. Infatti, i margini irrisori rimangono in mano all&#39;Associazione francese <em>Borsalinux, </em>che li user&agrave; per l&#39;amministrazione dell&#39;account condiviso.</span></span></p>
<p><span style="font-family: verdana, geneva, sans-serif"><span style="font-size: 14px">Procedendo verrai dirottato su un sito esterno a <a href="http://fedoraonline.it">http://fedoraonline.it</a>, altrimenti puoi tornare alla pagina principale di FOL.</span></span></p>
<div id="imgblock">
	<ul>
		<li class="imgupload">
			<span><a href="/">Home Fol</a></span></li>
		<li class="imggallery">
			<span><a href="http://fedora-fr.spreadshirt.net/it" target="_BLANK">Boutique</a></span></li>
	</ul>
</div>
<p>&nbsp;</p>
