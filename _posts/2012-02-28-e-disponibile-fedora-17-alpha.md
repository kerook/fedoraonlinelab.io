---
categories:
- staff news
layout: ultime_news
title: E' disponibile Fedora 17 - Alpha
created: 1330444261
---
<p>
	E&#39; stata appena rilasciata la versione Alpha di Fedora 17 - che nella sua versione finale porter&agrave; il nome <em>Beefy Miracle</em>.</p>
<p>
	Questo rilascio da un assaggio di quelle che saranno le novit&agrave; introdotte con F17, che uscir&agrave; nella versione finale i primi di maggio.</p>
<p>
	<a href="http://fedoraproject.org/get-prerelease"><span>http://fedoraproject.org/get-prerelease</span></a></p>
<p>
	La prima versione contiene tutte le novit&agrave; e invita gli utenti a collaborare per fixare i bug presenti. Una volta risolti questi errori uscir&agrave; la versione beta, la prima versione &quot;code-complete&quot;, nel senso che da l&igrave; in poi le modifiche che si faranno saranno davvero solo di bugfix. Il team di Fedora invita tutti al test della Alpha, invitando i tester ad aprire un ticket per eventuali bug scovati, proprio per realizzare una versione stabile in tutto e per tutto. Le maggiori novit&agrave; riguardano:</p>
<ul>
	<li>
		Gnome 3.3 (la versione finale sar&agrave; 3.4)</li>
	<li>
		Kde 4.8</li>
	<li>
		Gimp 2.8</li>
	<li>
		Kernel 3.3</li>
</ul>
<p>
	Il filesystem di default rimarr&agrave; ext4, in quanto btrfs &egrave; slittato almeno alla versione 18. Inoltre Gnome-Shell sar&agrave; in grado di funzionare anche con quelle schede grafiche che non supportano l&#39;accelerazione 3D, un enorme passo avanti per quanto riguarda l&#39;usabilit&agrave; del DE di default.</p>
<p>
	Da notare che Fedora 17 include anche il progetto &quot;Usrmove&quot;, ovvero lo spostamento di tutte le /lib e /bin sotto la directory /usr. Di fatto, le directory /lib, /lib64, /bin e /sbin conterranno soltanto dei link simbolici ai file che si troveranno sotto la directory /usr.</p>
<p>
	Buon test a tutti!</p>
