---
categories:
- staff news
layout: ultime_news
title: Mozilla ritira temporaneamente Firefox 16
created: 1349966869
---
<p>
	Soltanto un giorno dopo il rilascio di Firefox 16, Mozilla ha annunciato il ritiro temporaneo a causa di un bug che poteva, in particolari casi, consentire a siti malevoli di individuare gli URL visitati dagli utenti e i relativi parametri. Un nuovo update &egrave; atteso nel giro di pochi giorni.</p>
<p>
	Consigliamo, a chi lo avesse gi&agrave; aggiornato, di provvedere al downgrade dello stesso:</p>
<p>
	<span style="background-color: #ffd700"># yum downgrade firefox</span></p>
