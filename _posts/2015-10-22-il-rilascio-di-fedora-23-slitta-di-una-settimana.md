---
categories:
- staff news
layout: ultime_news
title: Il rilascio di Fedora 23 slitta di una settimana!
created: 1445535440
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p>Il responso del Go/No-Go Meeting appena conclusosi, ha decretato lo slittamento di una settimana nel rilascio di Fedora 23.</p>
<p>Qui il log della riunione: <a href="https://meetbot.fedoraproject.org/fedora-meeting-2/2015-10-22/f23-final-go_no_go-meeting.2015-10-22-16.00.log.html">https://meetbot.fedoraproject.org/fedora-meeting-2/2015-10-22/f23-final-go_no_go-meeting.2015-10-22-16.00.log.html</a></p>
<p>La causa &egrave; la presenza dei seguenti bug (considerati blocker), ancora da risolvere completamente:</p>
<ul>
	<li>
		<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1261569">https://bugzilla.redhat.com/show_bug.cgi?id=1261569</a></li>
	<li>
		<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1272737">https://bugzilla.redhat.com/show_bug.cgi?id=1272737</a></li>
	<li>
		<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1260989">https://bugzilla.redhat.com/show_bug.cgi?id=1260989</a></li>
	<li>
		<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1263677">https://bugzilla.redhat.com/show_bug.cgi?id=1263677</a></li>
	<li>
		<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1268802">https://bugzilla.redhat.com/show_bug.cgi?id=1268802</a></li>
	<li>
		<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1271061">https://bugzilla.redhat.com/show_bug.cgi?id=1271061</a></li>
	<li>
		<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1262600">https://bugzilla.redhat.com/show_bug.cgi?id=1262600</a></li>
	<li>
		<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1273214">https://bugzilla.redhat.com/show_bug.cgi?id=1273214</a></li>
</ul>
<p>Appuntamento a Gioved&igrave; prossimo per un altro Go/No-go!!!</p>
<p>&nbsp;</p>
