---
categories:
- staff news
layout: ultime_news
title: 'Annunciato il rilascio di Fedora 28'
created: 1526752648
---
<p><em>Nel corso della giornata che si sta avviando verso la conclusione, è stato reso disponibile il rilascio finale di Fedora 28.<br>
Di seguito si propone un adattamento italiano di un <a href="https://fedoramagazine.org/announcing-fedora-28/">recente articolo</a> di <strong>Matthew Miller</strong>, esaminando alcune delle principali novità, che accompagnano questa nuova versione.</em></p>
<p>&nbsp;</p>
<p><img alt="" src="https://fedoramagazine.org/wp-content/uploads/2018/04/fedora28-816x345.jpg" style="height:200px; width:473px"></p>
<p>&nbsp;</p>
<p><strong>INTRODUZIONE</strong></p>
<p>Ciao a tutti! È maggio e siamo attorno alla "festa della mamma", ricorrenza celebrata in svariate nazioni. Questo sta a significare che è tempo di un nuovo rilascio di Fedora: l’edizione 28 è qui tra di noi!</p>
<p>I link sono i seguenti, mentre maggiori dettagli sono disponibili più sotto:</p>
<ul><li><a href="https://getfedora.org/workstation/">Fedora 28 Workstation</a></li>
<li><a href="https://getfedora.org/server/">Fedora 28 Server</a></li>
<li><a href="https://getfedora.org/atomic/">Fedora 28 Atomic Host</a></li>
</ul><p>Per gli utenti che già utilizzano Fedora, è possibile effettuare un <a href="https://fedoramagazine.org/upgrading-fedora-27-fedora-28/">upgrade</a> dalla riga di comando o graficamente con GNOME Software. Sono stati compiuti numerosi sforzi per rendere il processo semplice e veloce. Nella maggioranza dei casi, esso richiederà circa una mezz’oretta, restituendo all’utente un sistema funzionante, senza grattacapi (personalmente, ieri notte ho lanciato i comandi dalla mia postazione primaria, subito prima di andare a dormire. Al mio risveglio, ho infine ritrovato un sistema perfettamente funzionante).</p>
<p>&nbsp;</p>
<p><strong>LE NOVITÀ DI FEDORA 28</strong></p>
<p>La novità più eclatante di Fedora 28 Server è l’avvenuta inclusione del nuovo repository Modular. Questo offre all’utente la possibilità di scegliere versioni differenti di software come NodeJS o Django. Si ha ora quindi la facoltà di personalizzare la dotazione utilizzata dal proprio software. Esiste della <a href="https://docs.fedoraproject.org/fedora-project/subprojects/fesco/en-US/Using_Modules.html">documentazione</a> dedicata all’argomento, per chi fosse interessato. Inoltre, l’architettura 64-bit ARM (Aarch64) è ora architettura primaria per Fedora Server.</p>
<p>Anche Fedora 28 Workstation ha numerose novità. Per la prima volta, si facilita l’attivazione di alcune sorgenti software di terze parti, come quelle relative ai driver proprietari Nvidia. Abbiamo lavorato a lungo per ideare il modo giusto per permettere ciò, senza compromettere i nostri ideali. Personalmente, penso che l’approccio corrente abbia raggiunto questo obiettivo. Il Fedora Magazine contiene articoli a riguardo di <a href="https://fedoramagazine.org/third-party-repositories-fedora/">repository di terze parti</a> e <a href="https://fedoramagazine.org/whats-new-fedora-28-workstation/">novità di F28 Workstation</a>.</p>
<p>Fedora Atomic Host aggiunge una delle caratteristiche a cui ambivo, nella mia precedente vita da sysadmin: aggiornamenti automatici. L’opzione è disattivata di default – si vedano il campo AutomaticUpdatePolicy per rpm-ostree e <a href="http://www.projectatomic.io/blog/2018/03/new-rpm-ostree-features/">questo post</a> per maggiori informazioni -. Fedora Atomic Host include Kubernetes 1.9 e lo strumento <a href="https://github.com/projectatomic/libpod">podman</a>.</p>
<p>Come sempre, sono presenti grandi novità, racchiuse all’interno di tutta la distribuzione. È possibile saperne di più, consultando le <a href="https://docs.fedoraproject.org/f28/release-notes/index.html">note di rilascio</a>.</p>
<p>&nbsp;</p>
<p><strong>EDIZIONI DI FEDORA</strong></p>
<p>Fedora Workstation è costruita su GNOME (ora giunto alla <a href="https://help.gnome.org/misc/release-notes/3.28/">versione 3.28</a>). Gli utenti interessati ad altri ambienti desktop popolari come KDE, Xfce e Cinnamon, possono consultare la pagina relativa alle <a href="https://spins.fedoraproject.org/">Fedora Spins</a>. Per versioni adattate ad usi specifici - come astronomia, design, sicurezza informatica, robotica, insegnamento di Python – occorre consultare la home page dei <a href="https://labs.fedoraproject.org/">Fedora Labs</a>. Esiste inoltre <a href="https://cloud.fedoraproject.org/">Fedora Cloud Base</a>, dedicata a chi ha bisogno di un ambiente da strutturare con tecnologie cloud come, ad esempio, EC2 e OpenStack. Abbiamo poi network installers, architetture alternative (come Power e 64-bit ARM), link Torrent e diversi <a href="https://alt.fedoraproject.org/">altri download differenti</a>. Non dimentichiamoci poi della pagina <a href="https://arm.fedoraproject.org/">Fedora ARM</a>, per chi ha l’intenzione di installare Fedora su una scheda Raspberry Pi o altri dispositivi di tipo ARM.</p>
<p>Fedora è attiva su molti fronti, perché siamo un progetto con un gran numero di persone con diversi interessi. Se l’utente non trova ciò a cui aspira, può <a href="https://fedoraproject.org/wiki/Join">partecipare al Fedora Project</a> e lavorare assieme a noi per creare ciò che desidera. La nostra missione è quella di costruire una piattaforma che dia la possibilità ai contributori e agli altri sviluppatori, di risolvere tutti i tipi di problemi d’utilizzo, rimanendo sempre legati ai nostri <a href="https://docs.fedoraproject.org/fedora-project/project/fedora-overview.html">principi</a> di “Freedom (libertà), Friendship (amicizia), Features (caratteristiche, contenuti), e First (avanguardia)”.</p>
<p>&nbsp;</p>
<p><strong>RINGRAZIAMENTI</strong></p>
<p>Ancora una volta, questo rilascio si presenta come il migliore in assoluto. Non è una vera sorpresa, perché è il risultato di dedizione, impegno e molte ore di lavoro duro da parte di migliaia di persone. Ringrazio veramente tanto questa meravigliosa comunità, per aver reso possibile l’uscita di questa nuova versione.</p>
<p>Oh… Un’ultima cosa… Siamo umani: perfino il prodotto migliore, non può essere perfetto. Ci sono sempre casi particolari e problematiche dell’ultima ora. L’utenza che riscontra qualcosa di strano, può consultare la lista dei <a href="https://fedoraproject.org/wiki/Common_F28_bugs">Bugs comuni per F28</a>. Invito, chi trova un problema, ad aiutarci a migliorare le cose.</p>
<p>Godiamoci Fedora 28!</p>
