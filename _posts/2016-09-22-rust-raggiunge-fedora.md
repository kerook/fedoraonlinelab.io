---
categories:
- staff news
layout: ultime_news
title: Rust raggiunge Fedora
created: 1474573365
---
<p class="rtejustify"><img alt="" src="https://i2.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/rust.jpg?resize=945%2C400&amp;ssl=1" style="height: 150px; width: 354px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">Il presente articolo, &egrave; un adattamento in italiano dell&#39;<a href="https://fedoramagazine.org/rust-meets-fedora/">originale</a> scritto da Sumantro Mukherjee per Fedora Magazine.</p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><strong>Cos&#39;&egrave; Rust?</strong><br />
	<br />
	Rust &egrave; un linguaggio di sistema, le cui istruzioni vengono eseguite in maniera veloce e prevengono (quasi interamente) crash e segfaults. L&#39;intento di questo articolo, &egrave; quello di spiegare l&#39;utilit&agrave; nell&#39;adozione di questo nuovo linguaggio di programmazione, nonostante la presenza di molti altri &quot;concorrenti&quot;.<br />
	<br />
	<strong>Sicurezza o controllo?</strong></p>
<p class="rtejustify"><strong><img alt="" src="https://i0.wp.com/cdn.fedoramagazine.org/wp-content/uploads/2016/09/Screenshot-from-2016-09-12-08-29-02.png?resize=300%2C210&amp;ssl=1" style="width: 200px; height: 140px;" /></strong><br />
	<br />
	Un diagramma simile a quello qui mostrato &egrave; molto comune. Da un lato, abbiamo C e C++, che hanno un maggior controllo nei confronti dell&#39;hardware su cui vengono eseguito. Di conseguenza, essi offrono allo sviluppatore la possibilit&agrave; di ottimizzare i livelli di performance. Tutto ci&ograve; per&ograve;, non evidenzia un alto livello di sicurezza, in quanto pu&ograve; causare segfaults o bug critici come il famoso Heartbleed.<br />
	<br />
	D&#39;altro canto, esistono linguaggi come Python, Ruby e JavaScript dove il programmatore ha meno possibilit&agrave; di controllo, ma genera codice meno pericoloso, che non causa segfaults - bug ed eccezioni esistono, ma sono pi&ugrave; sicure e pi&ugrave; contenute -.<br />
	<br />
	Nel mezzo, &egrave; possibile trovare Java e alcuni altri linguaggi, che presentano un amalgama di queste caratteristiche, minimizzando le vulnerabilit&agrave; pur offrendo un certo grado di controllo sull&#39;hardware.<br />
	<br />
	Rust &egrave;, in un certo senso, differente, in quanto non rientra pienamente nello schema illustrato. Esso cerca infatti di offrire sicurezza, senza rinunciare al controllo.<br />
	<br />
	<br />
	<strong>Peculiarit&agrave; di Rust</strong><br />
	<br />
	Rust &egrave; un linguaggio di programmazione di sistema esattamente come C/C++, ma offre allo sviluppatore un preciso e completo controllo sull&#39;allocazione della memoria. Un garbage collector non &egrave; richiesto. Ha una runtime minimale e viene eseguito ad un livello molto basso. Il programmatore ha quindi una certa confidenza a riguardo delle performance del codice. Inoltre, chiunque conosca C/C++, pu&ograve; capire e scrivere codice Rust.<br />
	<br />
	Essendo un linguaggio compilato, la velocit&agrave; &egrave; appunto assicurata. Come backend, viene usato LLVM che consente un certo numero di ottimizzazioni, che in termini di performance riescono a far prevalere gli applicativi Rust rispetto a quelli C/C++. Il livello di sicurezza &egrave; pari a quello di JavaScript, Ruby e Python, in quanto il linguaggio non ammette l&#39;esistenza di segfaults e di problemi causati da puntatori.<br />
	<br />
	Un&#39;altra caratteristica importante &egrave; l&#39;eliminazione del problema della concorrenzialit&agrave; dei dati. Al giorno d&#39;oggi, la maggior parte dei computer hanno processori multi-core e molti thread vengono eseguiti in parallelo. Il tutto per&ograve;, richiede numerosi sforzi da parte dello sviluppatore. Le caratteristiche di Rust consentono di rimuovere la necessit&agrave; di ricorrere alla programmazione multitasking. Ci sono due concetti chiave che vengono usati per risolvere la concorrenzialit&agrave; dei dati:<br />
	<br />
	&nbsp;&nbsp;&nbsp; <em>Ownership (propriet&agrave;)</em>. Ogni variabile viene spostata in una nuova posizione, senza consentire alle posizioni precedenti di usarla. Ogni dato &egrave; posseduto da un solo proprietario<br />
	&nbsp;&nbsp;&nbsp; <em>Borrowing (prestito)</em>. Valori posseduti, possono esssere &quot;prestati&quot; per consentire l&#39;utilizzo per un certo periodo di tempo.<br />
	<br />
	<br />
	<strong>Rust in Fedora 24 e 25</strong><br />
	<br />
	Per utilizzare Rust, &egrave; sufficiente installare il seguente pacchetto:<br />
	<br />
	<em># dnf install rust</em><br />
	<br />
	Viene quindi riportato un programma demo, che &egrave; possibile ricopiare per sperimentare il linguaggio. &Egrave; sufficiente salvare un file &quot;helloworld.rs&quot;, con il seguente contenuto:<br />
	<br />
	<em>fn main() {<br />
	&nbsp;&nbsp;&nbsp; println!(&quot;Hello, Rust is running on Fedora 25 Alpha!&quot;);<br />
	}</em><br />
	<br />
	Per compilare, bisogna usare &quot;rustc&quot;. L&#39;eseguibile pu&ograve; quindi essere eseguito come un qualsiasi software:<br />
	<br />
	<em>$ rustc helloworld.rs<br />
	$ ./helloworld</em><br />
	<br />
	<strong>Contribuire al testing di Rust</strong><br />
	<br />
	Per installare la pi&ugrave; recente versione di testing su Fedora, occorre eseguire il seguente comando:<br />
	<br />
	<em># dnf --enablerepo=updates-testing --refresh --best install rust</em><br />
	<br />
	In caso di dubbi, pu&ograve; essere utile contattare il Fedora Quality Assurance, via mail (<a href="mailto:test@lists.fedoraproject.org">test@lists.fedoraproject.org</a>) o via IRC (#fedora-qa su Freenode).</p>
