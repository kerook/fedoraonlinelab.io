---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 17 beta
created: 1334696859
---
<em>Guarda nel cielo! E&#39; un uccello, &egrave; un aereo, &egrave;....la beta di Beefy Miracle!</em><br />

Con questa introduzione oggi &egrave; stata ufficialmente rilasciata la beta di Fedora 17, scaricabile da qui:
<a href="http://fedoraproject.org/get-prerelease">http://fedoraproject.org/get-prerelease</a> 

Anche se si tratta dell&#39;ultima versione prima del rilascio finale, si tratta pur sempre di una beta, quindi vi consigliamo di leggere sempre i bug noti:
<a href="http://fedoraproject.org/wiki/Common_F17_bugs">http://fedoraproject.org/wiki/Common_F17_bugs</a>
<p>
	Caratteristiche:</p>
<ul>
	<li>
		Gnome 3.4</li>
	<li>
		Juno</li>
	<li>
		OpenJDK 7</li>
	<li>
		Php 5.4</li>
	<li>
		Kernel 3.3</li>
	<li>
		Supporto GMA (poulsbo)</li>
	<li>
		Supporto Broadcom</li>
	<li>
		Firewalld</li>
</ul>
ecc ecc, ulteriori informazioni sono disponibili qui:
<a href="http://fedoraproject.org/wiki/Releases/17/FeatureList">http://fedoraproject.org/wiki/Releases/17/FeatureList</a>

Inoltre, per chi vuole sperimentare fin da subito, sono gi&agrave; disponibili moltissime spin:
<a href="http://dl.fedoraproject.org/pub/alt/nightly-composes/">http://dl.fedoraproject.org/pub/alt/nightly-composes/</a>

Inutile dire che siete tutti invitati a provare questa versione per aiutare la comunit&agrave; nelle sistemazione di eventuali bug. Tutto il processo di testing e di bugfixing fa parte dei vari gruppi di contributors, bugzappers ecc, quindi direi che &egrave; l&#39;occasione giusta per sottolineare l&#39;importanza di contribuire alla nostra distro e proprio per questo abbiamo da poco inserito dei nuovi forum. Buon test a tutti e soprattutto, buon divertimento!

Lo staff di Fedora Online
