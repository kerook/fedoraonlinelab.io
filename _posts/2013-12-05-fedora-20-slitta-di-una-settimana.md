---
categories:
- staff news
layout: ultime_news
title: Fedora 20 slitta di una settimana
created: 1386278020
---
<p>E&#39; stato appena deciso, ma si sapeva da qualche giorno, che Fedora 20 non uscir&agrave; il 10 dicembre come previsto ma il 17, salvo ulteriori spostamenti.</p>
<p>Il 17 dicembre &egrave; anche l&#39;ultima possibilit&agrave; che abbiamo di vedere F20 ancora nel 2013, se non dovesse essere pronta per il prossimo gioved&igrave;, data della decisione del GO/NO GO, si va direttamente a gennaio 2014.</p>
<p>Per chi &egrave; interesato pu&ograve; leggersi la lista dei bug qui: <a href="http://qa.fedoraproject.org/blockerbugs/milestone/20/final/buglist">http://qa.fedoraproject.org/blockerbugs/milestone/20/final/buglist</a></p>
<p>La Release Schedule aggiornata &egrave; consultabile qui: <a href="https://fedoraproject.org/wiki/Releases/20/Schedule">https://fedoraproject.org/wiki/Releases/20/Schedule</a></p>
