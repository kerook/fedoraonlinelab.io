---
categories:
- staff news
layout: ultime_news
title: 'Test Days F23: oggi in programma l''evento relativo a Cinnamon'
created: 1444284464
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p class="rtejustify"><br />
	&Egrave; arrivato l&#39;annuncio ufficiale della data relativa al <a href="http://doc.fedoraonline.it/TestDays">Test Day</a> della nuova spin <a href="https://fedoraproject.org/wiki/Test_Day:2015-10-08_Cinnamon">Cinnamon</a>. L&#39;evento si terr&agrave; nella giornata di oggi, 08 Ottobre 2015. Si ricorda che &egrave; comunque possibile inserire i propri risultati anche nei giorni successivi alla conclusione.<br />
	<br />
	<strong>Preparazione del sistema</strong>:<br />
	&Egrave; necessario utilizzare un computer con una scheda grafica capace di supportare OpenGL. Per questo specifico evento, non si pul teoricamente usufruire di una macchina virtuale, ma ogni tipo di collaborazione &egrave; ben accetta.<br />
	A breve, verr&agrave; reso disponibile un apposito file ISO per la creazione di una live USB.<br />
	Alternativamente, sar&agrave; possibile installare fisicamente la beta di Fedora 23 Cinnamon, avendo per&ograve; cura di applicare tutti gli aggiornamenti disponibili prima di eseguire le prove.<br />
	<br />
	<strong>Casi da testare</strong>:<br />
	I casi da testare sono elencati nella <a href="https://fedoraproject.org/wiki/Test_Day:2015-10-08_Cinnamon">pagina dedicata alla giornata</a>, la quale contiene anche una tabella per riepilogare i risultati.<br />
	Stabilit&agrave; del browser, funzionamento del terminale, e test dell&#39;audio sono alcuni esempi di verifiche da effettuare.<br />
	Nonostante i passaggi da seguire siano ben precisi e ben definiti, qualsiasi tipo di prova &egrave; la benvenuta.<br />
	<br />
	<strong>IRC</strong>:<br />
	Confermato il canale <a href="irc://irc.freenode.net/#fedora-test-day">#fedora-test-day</a></p>
