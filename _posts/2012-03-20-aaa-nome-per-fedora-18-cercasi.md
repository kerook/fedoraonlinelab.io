---
categories:
- staff news
layout: ultime_news
title: AAA nome per Fedora 18 cercasi
created: 1332229685
---
<p>
	Ormai Fedora 17, nome in codice Beefy Miracle, ha iniziato il suo percorso verso la versione finale e quindi &egrave; gi&agrave; ora di pensare al nome di Fedora 18.</p>
<p>
	Le proposte possono essere fatte da tutta la comunit&agrave; a partire <strong>da oggi 20 marzo 2012 e non oltre la mezzanotte del 27 marzo</strong>: poco tempo quindi per pensare a un nome da dare alla prossima release. Le linee guida, che sono reperibili cliccando su:<br />
	<a href="http://fedoraproject.org/wiki/Name_suggestions_for_Fedora_18">http://fedoraproject.org/wiki/Name_suggestions_for_Fedora_18</a></p>
<p>
	sono essenzialmente le seguenti:</p>
<ul>
	<li>
		ci deve essere un collegamento &quot;is a&quot; (&egrave; un) tra Beefy Miracle e il nome suggerito (esempio: Verne &egrave; uno scrittore, anche Karl May lo &egrave;)</li>
	<li>
		questo collegamento deve essere diverso dai collegamenti utilizzati tra i nomi di versioni precedenti</li>
	<li>
		Nomi di persone viventi e marchi ben conosciuti saranno scartati a priori</li>
</ul>
<p>
	Con un po&#39; di fantasia, quindi, il fedoraproject si aspetta di ricevere molte nuove segnalazioni e collegamenti simpatici. Chi vuole proporre il suo?</p>
