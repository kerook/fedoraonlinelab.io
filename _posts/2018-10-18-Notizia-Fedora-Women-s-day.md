---
categories:
- staff news
layout: ultime_news
title: 'Fedora Women’s Day 2018'
created: 1539980343
---

Buongiorno a tutti!

Quest'oggi a Monaco di Baviera, si è svolto il loro primo "[Fedora Women's Day 2018](https://pagure.io/fedora-diversity/fedora-womens-day/issue/13)". 
In Italia, per quest'anno, era già stato organizzato, il 29 settembre scorso, a Trieste. 

Lo scopo di questa giornata, come riportato sulla [pagina relativa](https://fedoraproject.org/wiki/Category:Fedora_Women%27s_Day?rd=Fedora_Women%27s_Day) sulla Wiki di Fedora, è: 

> Fedora Women's Day (FWD) is a day of celebration to help raise awareness and thanks for the women contributors across the Fedora Project. Fedora Women's Day marks the anniversary of the Fedora Women team.

cioè una giornata che punta a favorire il coinvolgimento delle donne (cis e trans) e genderqueer nel progetto Fedora e del Software Libero e Open Source, per aumentarne la consapevolezza e ringraziare quelle che già contribuiscono al progetto e magari coinvolgerne altre!

Il progetto sembra molto interessante, non sarebbe bello organizzarne uno nazionale per il prossimo anno?


