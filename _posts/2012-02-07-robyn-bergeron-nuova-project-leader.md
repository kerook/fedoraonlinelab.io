---
categories:
- staff news
layout: ultime_news
title: Robyn Bergeron nuova Project Leader
created: 1328627661
---
<p>
	E&#39; appena stato reso noto che <span>Robyn Bergeron sar&agrave; la nuova Project Leader del fedoraproject.</span> Ha contribuito attivamente negli ultimi anni al progetto e si &egrave; conquistata la massima stima da parte di molti componenti all&#39;interno del progetto. Lo annuncia l&#39;attuale Project Leader Jared Smith, che collaborer&agrave; nei prossimi mesi con la nuova &quot;capa&quot; per introdurla al meglio.<br />
	Di seguito riportiamo l&#39;annuncio originale di Jared:</p>
<p>
	<em><span>One of the things I like most about the Fedora Project is the</span><br />
	<span>opportunity for people to move and grow in (and out) of different</span><br />
	<span>roles and responsibilities. &nbsp;&nbsp;The position of Fedora Project Leader,</span><br />
	<span>in particular, has never been a long-term leadership position, but one</span><br />
	<span>that regularly invites new people to assume the role and bring new</span><br />
	<span>ideas and new energy to the project. &nbsp;I would like to take this</span><br />
	<span>opportunity to share some of my thoughts about being the Fedora</span><br />
	<span>Project Leader, and inform you of upcoming changes in Fedora</span><br />
	<span>leadership. &nbsp;Any time we make leadership changes in Fedora, we that</span><br />
	<span>that challenge seriously, and do everything we can to make the</span><br />
	<span>leadership transition as smooth as possible.</span></em><br />
	<br />
	<em><span>Although I&#39;ve been using Fedora since the split from Red Hat Linux,</span><br />
	<span>it&#39;s only been the past five of six years that I&#39;ve really been an</span><br />
	<span>active contributor. &nbsp;Sure, I was hanging out on the mailing lists,</span><br />
	<span>trying out the pre-releases and reporting bugs, but I didn&#39;t really</span><br />
	<span>consider myself a part of Fedora. &nbsp;It wasn&#39;t until I got started with</span><br />
	<span>the Docs team and attended my first FUDCon that I truly caught the</span><br />
	<span>spirit of the Fedora community. &nbsp;Since then, I&#39;ve thoroughly enjoyed</span><br />
	<span>rubbing shoulders with people who are infinitely smarter than me, and</span><br />
	<span>I&#39;ve learned a tremendous amount -- both about the technical bits and</span><br />
	<span>bytes, and also about free software communities. &nbsp;And for the last</span><br />
	<span>little while, it&#39;s been my honor and privilege to serve the community</span><br />
	<span>as the Fedora Project Leader. &nbsp;The role of Fedora Project Leader isn&#39;t</span><br />
	<span>an easy role, but I am proud of the things we&#39;ve been able to</span><br />
	<span>accomplish both within the distribution and within the community</span><br />
	<span>during my tenure. &nbsp;We&#39;ve had three solid Fedora releases during my</span><br />
	<span>time as FPL, each one with a myriad of new features. &nbsp;I&#39;ve worked hard</span><br />
	<span>to expand our international outreach, and to get more international</span><br />
	<span>representation on the Fedora Board. &nbsp;We&#39;ve updated the Fedora website.</span><br />
	<span>We&#39;ve improved our quality assurance processes. &nbsp;We&#39;ve been able to</span><br />
	<span>deliver Fedora images for the Amazon EC2 cloud on release day. &nbsp;We&#39;ve</span><br />
	<span>improved our translation system. &nbsp;I&#39;m thankful for all those who have</span><br />
	<span>worked hard to help drive Fedora forward. &nbsp;Now is the time for me to</span><br />
	<span>pass the torch to the next Fedora Project Leader.</span></em><br />
	<br />
	<em><span>As you probably already know, Red Hat employs the FPL to ensure</span><br />
	<span>someone is accountable to Red Hat and the rest of the community for</span><br />
	<span>the Fedora Project as a whole. &nbsp;After all, many Fedora leaders have</span><br />
	<span>referred to the FPL as &quot;the one throat to choke&quot; when it comes to</span><br />
	<span>Fedora. &nbsp;The FPL is still subject to the same process as any other Red</span><br />
	<span>Hat hire, though, and ultimately Red Hat is responsible for that</span><br />
	<span>decision. &nbsp;It is imperative that the decision be a good one for the</span><br />
	<span>entire Fedora community, so the Fedora Board is consulted about the</span><br />
	<span>selection. &nbsp;This process has continued to work well for several</span><br />
	<span>previous FPLs, and the Board provided positive feedback about our</span><br />
	<span>selection this time around, too.</span></em><br />
	<br />
	<em><span>I&#39;m happy to announce that Red Hat has selected Robyn Bergeron to be</span><br />
	<span>the next Fedora Project Leader. &nbsp;Robyn has proven herself in the</span><br />
	<span>Fedora community over the last several years, and I have complete</span><br />
	<span>confidence in her abilities to lead the Fedora Project. &nbsp;In addition</span><br />
	<span>to planning FUDCon Tempe in 2011 and helping to lead the Marketing and</span><br />
	<span>Cloud SIGs within Fedora, Robyn has been an integral part of many</span><br />
	<span>other Fedora events and endeavors. &nbsp;Most recently, she has held the</span><br />
	<span>role of Fedora Program Manager, helping to ensure that we all stay on</span><br />
	<span>schedule and helping the Fedora feature process stay on track. &nbsp;Please</span><br />
	<span>join with me in welcoming Robyn into her new role, and in giving her</span><br />
	<span>your help and support in her new role. &nbsp;I&#39;ll be working with Robyn</span><br />
	<span>over the next weeks and months to help her in the new role.</span></em><br />
	<br />
	<span>--</span><br />
	<em><span>Jared Smith</span><br />
	<span>Former Fedora Project Leader</span></em></p>
