---
categories:
- staff news
layout: ultime_news
title: 'Fedora 18 EOL: 14 gennaio 2014'
created: 1387724552
---
<p>Come di consueto, un mese dopo il rilascio di una nuova versione Fedora, la versione n-2 raggiunge la fine del suo ciclo di vita (End of Life).</p>
<p>Stavolta tocca a Fedora 18, che con il 14 gennaio 2014 non verr&agrave; pi&ugrave; mantenuta e di conseguenza &egrave; da considerare legacy, perch&egrave; priva di ulteriori aggiornamenti. Consigliamo vivamente di aggiornare eventuali macchine con Fedora 18 a Fedora 20.</p>
