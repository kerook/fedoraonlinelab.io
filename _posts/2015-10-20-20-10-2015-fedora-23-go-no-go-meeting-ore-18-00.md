---
categories:
- staff news
layout: ultime_news
title: 20/10/2015 - Fedora 23 Go/No-go Meeting (ore 18.00)
created: 1445340257
---
<p class="rtejustify"><img alt="" src="https://getfedora.org/static/images/fedora_infinity_140x140.png" style="width: 100px; height: 100px;" /></p>
<p class="rtejustify">Questo gioved&igrave; sera, alle 18:00 (ora Italiana), si terr&agrave; il Go/No-Go Meeting internazionale, relativo a Fedora 23.</p>
<p class="rtejustify">Prima di ogni rilascio infatti, i team Development, QA e Release Engineering si riuniscono per determinare se i criteri di rilascio previsti sono stati raggiunti.</p>
<p class="rtejustify">L&#39;obiettivo, &egrave; quello di decidere se certificare la versione come &quot;Gold&quot; e rilasciarla per il giorno previsto (Marted&igrave; 27 Ottobre 2015)</p>
<p class="rtejustify">L&#39;incontro, ovviamente aperto a tutti, si svolger&agrave; nel canale IRC &quot;#fedora-meeting-2&quot;</p>
<p class="rtejustify">Di seguito, ecco alcuni links utili:</p>
<ul>
	<li class="rtejustify">
		Annuncio ufficiale di Jan Kurik (Platform &amp; Fedora Program Manager): <a href="https://lists.fedoraproject.org/pipermail/test-announce/2015-September/001122.html">https://lists.fedoraproject.org/pipermail/test-announce/2015-September/001122.html</a></li>
	<li class="rtejustify">
		Informazioni relative al Go/No-Go Meeting: <a href="https://fedoraproject.org/wiki/Go_No_Go_Meeting">https://fedoraproject.org/wiki/Go_No_Go_Meeting</a></li>
	<li class="rtejustify">
		Criteri per il rilascio di Fedora 23:<a href="http://https://fedoraproject.org/wiki/Fedora_23_Final_Release_Criteria"> https://fedoraproject.org/wiki/Fedora_23_Final_Release_Criteria</a></li>
	<li class="rtejustify">
		Lista aggiornata dei bug bloccanti per il rilascio: <a href="https://qa.fedoraproject.org/blockerbugs/milestone/23/final/buglist">https://qa.fedoraproject.org/blockerbugs/milestone/23/final/buglist</a></li>
	<li class="rtejustify">
		Release schedule: <a href="https://fedoraproject.org/wiki/Releases/23/Schedule">https://fedoraproject.org/wiki/Releases/23/Schedule</a></li>
</ul>
