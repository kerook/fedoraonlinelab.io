---
categories:
- staff news
layout: ultime_news
title: Fedora 20 Beta prevista per il 12 novembre
created: 1383300823
---
<p>No Release Candidate, no Party (Beta)!</p>
<p>Fedora 20 Beta slitta quindi di un&#39;altra settimana, visto che la mancanza di gente disponibile ha impedito di farla ritardare solo di uno o due giorni. I bug ancora irrisolti sono consultabili qui:</p>
<p><a href="https://qa.fedoraproject.org/blockerbugs/milestone/20/beta/buglist">https://qa.fedoraproject.org/blockerbugs/milestone/20/beta/buglist</a></p>
<p>Visto che siamo molto a ridosso di Natale, &egrave; stato deciso di sottoporre al FESCO la richiesta di mantenere eventualmente la data di rilascio al 10 dicembre, accorciando di fatto il periodo tra Beta e GA. Per ora nella Release Schedule &egrave; ovviamente segnalato il 17 come data della Final Release, tenetela d&#39;occhio perch&egrave; potrebbe cambiare. In ogni caso FOL vi informer&agrave; su eventuali cambiamenti, anche attraverso G+ (comunit&agrave; Fedora Italia).</p>
<p><a href="https://fedoraproject.org/wiki/Releases/20/Schedule">https://fedoraproject.org/wiki/Releases/20/Schedule</a></p>
