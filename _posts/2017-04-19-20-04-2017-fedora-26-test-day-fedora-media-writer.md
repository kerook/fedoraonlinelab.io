---
categories:
- staff news
layout: ultime_news
title: '20/04/2017: Fedora 26 Test Day (Fedora Media Writer)'
created: 1492589952
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p>&nbsp;</p>
<p><strong>Introduzione</strong></p>
<p>Nella giornata di Gioved&igrave; 20 Aprile 2017, &egrave; prevista una giornata di testing.<br />
	<br />
	L&#39;obiettivo sar&agrave; quello di sperimentare le funzionalit&agrave; di Fedora Media Writer all&#39;interno di diversi sistemi operativi e architetture. Tale strumento &egrave; fornito come opzione primaria per il download di Fedora Workstation, con lo scopo di facilitare la prova e l&#39;installazione di Fedora.<br />
	<br />
	La pagina relativa all&#39;evento &egrave; la seguente: <a href="https://fedoraproject.org/wiki/Test_Day:2017-04-20_Fedora_Media_Writer">https://fedoraproject.org/wiki/Test_Day:2017-04-20_Fedora_Media_Writer</a><br />
	<br />
	&nbsp;<br />
	<strong>Preparazione del sistema</strong>:<br />
	<br />
	- Occorre avere una chiavetta USB da almeno 4 GB e un computer sul quale poter provare la Live.<br />
	<br />
	- Sono necessari i privilegi amministrativi all&#39;interno del sistema operativo usato per creare il supporto.<br />
	<br />
	- La versione pi&ugrave; recente di Fedora Media Writer (si veda la sezione seguente).<br />
	<br />
	<br />
	<strong>Come testare</strong>:<br />
	<br />
	&Egrave; sufficiente installare il tool richiesto.<br />
	<br />
	- All&#39;interno di Fedora:<br />
	<strong><em># dnf --enablerepo=updates-testing --refresh --best install mediawriter</em></strong></p>
<p>- All&#39;interno di Windows e Mac Os: prelevare <a href="https://github.com/MartinBriza/MediaWriter/releases/">qui</a> il rilascio pi&ugrave; recente.<br />
	<br />
	<strong>&nbsp;&nbsp; &nbsp;<br />
	Casi da testare</strong>:<br />
	<br />
	In linea di massima, occorre configurare una chiavetta per l&#39;installazione del prossimo rilascio di Fedora. I passaggi sono delineati <a href="https://fedoraproject.org/wiki/QA:Testcase_USB_fmw">qui.</a></p>
<p><br />
	<strong>IRC</strong>:<br />
	<br />
	Il canale internazionale ufficiale per l&#39;evento &egrave; il consueto #fedora-test-day.</p>
