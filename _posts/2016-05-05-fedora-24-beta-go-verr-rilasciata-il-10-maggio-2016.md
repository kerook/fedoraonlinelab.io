---
categories:
- staff news
layout: ultime_news
title: 'Fedora 24 Beta è GO: verrà rilasciata il 10 Maggio 2016!'
created: 1462481212
---
<p class="rtejustify"><img alt="Desktop di Fedora 24 Alpha" src="http://juliux.altervista.org/images/desktops/fedora/f24_alpha.png" style="width: 400px; height: 225px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Nel corso del Go/No-Go Meeting odierno, relativo a Fedora 24 Beta, i team QA, Release Engineering e Development hanno deciso di dare il via libera al rilascio.</span></p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Fedora 24 Beta sar&agrave; quindi pubblicamente disponibile il giorno 10 Maggio 2016.</span></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify"><span style="color: rgb(46, 52, 54); font-family: Monospace; font-size: 15px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">Log della riunione: <a href="https://meetbot.fedoraproject.org/fedora-meeting-1/2016-05-05/f24-beta-go_no_go-meeting.2016-05-05-17.30.log.html">https://meetbot.fedoraproject.org/fedora-meeting-1/2016-05-05/f24-beta-go_no_go-meeting.2016-05-05-17.30.log.html</a></span></p>
<p class="rtejustify">&nbsp;</p>
