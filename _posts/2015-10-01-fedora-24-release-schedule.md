---
categories:
- staff news
layout: ultime_news
title: 'Fedora 24: Release Schedule'
created: 1443713586
---
<p>&Egrave; online il programma di rilascio della prossima <strong>Fedora 24</strong>.</p>
<p>Queste le date previste:</p>
<ul>
	<li>
		Alpha&nbsp; 01 marzo 2016</li>
	<li>
		Beta&nbsp;&nbsp; 12 aprile 2016</li>
	<li>
		Final&nbsp; 17 maggio 2016</li>
</ul>
<p>&nbsp;</p>
<p>Tra le novit&agrave; gi&agrave; accettate per la prossima Fedora avremo il pacchetto TeXLive 2015</p>
<p>&nbsp;</p>
<p>A questo link la Release Schedule completa:</p>
<p>https://fedoraproject.org/wiki/Releases/24/Schedule</p>
<p>Stay Tuned!</p>
