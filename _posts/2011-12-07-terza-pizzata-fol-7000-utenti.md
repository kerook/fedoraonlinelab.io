---
categories:
- staff news
layout: ultime_news
title: 'Terza pizzata Fol: 7000 utenti'
created: 1323294386
---
<p>
	Il sondaggio ha praticamente decretato il luogo dove tenere la terza pizzata di Fedora Online.</p>
<p>
	Non ci sbilanciamo ancora sul luogo, ma sembra evidente che il ballottaggio sia <strong>tra Roma e Napoli</strong>, anche se su una delle due pesano molti doppi voti.</p>
<p>
	Visto che siamo sotto Natale e sarebbe difficile organizzare una serata in cos&igrave; poco tempo, &egrave; certo che ci incontreremo in gennaio, anche se nel frattempo avremo superato i 7000 utenti. L&#39;evento coincider&agrave; con il 7&deg; compleanno di Fol e la presentazione di un&#39;altra novit&agrave; molto interessante...</p>
<p>
	Continuate a seguirci per i prossimi aggiornamenti.</p>
<p>
	Lo staff di Fedora Online</p>
<p>
	&nbsp;</p>
