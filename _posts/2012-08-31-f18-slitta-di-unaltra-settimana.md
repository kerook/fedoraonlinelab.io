---
categories:
- staff news
layout: ultime_news
title: F18 slitta di un'altra settimana
created: 1346416237
---
<p>
	Come da previsioni, e confermato oggi nella ML, il rilascio della Alpha di Fedora 18, e di conseguenza anche della versione finale, &egrave; stato ritardato di un&#39;altra settimana:</p>
<p>
	http://lists.fedoraproject.org/pipermail/devel-announce/2012-August/000970.html</p>
<p>
	Al Go/No-Go meeting &egrave; stato deciso di far slittare di una settimana il rilascio di Fedora 18 Alpha a causa di numerosi bug irrisolti [1] e test non completi [2][3][4] e si teme che ci possano essere ulteriori rinvii. La causa principale &egrave; un bug di Anaconda, legato all&#39;upgrade dei sistemi F17 e che per adesso non ha avuto una soluzione soddisfacente. Di conseguenza gli sviluppatori stessi in questo momento parlano di un ritardo rispetto alla Release Schedule di almeno tre settimane invece delle attuali due.</p>
<p>
	Per maggiori dettagli si pu&ograve; fare riferimento al log del meeting [5].</p>
<p>
	All&#39;annuncio dello sviluppatore sono stati segnalati i prossimi meeting importanti per il futuro di Fedora 18.</p>
<p>
	[1] <a href="http://qa.fedoraproject.org/blockerbugs/current" target="_blank">http://qa.fedoraproject.org/<wbr />blockerbugs/current</a><br />
	[2] <a href="https://fedoraproject.org/wiki/Test_Results:Current_Base_Test" target="_blank">https://fedoraproject.org/<wbr />wiki/Test_Results:Current_<wbr />Base_Test</a><br />
	[3] <a href="http://fedoraproject.org/wiki/Test_Results:Current_Installation_Test" target="_blank">http://fedoraproject.org/wiki/<wbr />Test_Results:Current_<wbr />Installation_Test</a><br />
	[4] <a href="http://fedoraproject.org/wiki/Test_Results:Current_Desktop_Test" target="_blank">http://fedoraproject.org/wiki/<wbr />Test_Results:Current_Desktop_<wbr />Test</a><br />
	[5] <a href="http://bit.ly/PC16PF" target="_blank">http://bit.ly/PC16PF</a></p>
