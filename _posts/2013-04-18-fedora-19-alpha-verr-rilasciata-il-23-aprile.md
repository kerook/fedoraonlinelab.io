---
categories:
- staff news
layout: ultime_news
title: Fedora 19 Alpha verrà rilasciata il 23 aprile
created: 1366313498
---
<p>E&#39; stato appena deciso che la Alpha di Schroedinger&#39;s Cat, ovvero Fedora 19, verr&agrave; &nbsp;rilasciata come previsto il 23 aprile.</p>
<p>Per chi &egrave; interessato a leggersi il log del meeting, eccolo:
<a href="http://meetbot.fedoraproject.org/fedora-meeting-2/2013-04-18/f19_alpha_gono-go_meeting.2013-04-18-17.00.log.html">http://meetbot.fedoraproject.org/fedora-meeting-2/2013-04-18/f19_alpha_gono-go_meeting.2013-04-18-17.00.log.html</a></p>
