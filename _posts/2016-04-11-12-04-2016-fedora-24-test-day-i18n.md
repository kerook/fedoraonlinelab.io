---
categories:
- staff news
layout: ultime_news
title: '12/04/2016: Fedora 24 Test Day (i18n)'
created: 1460354777
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p>&nbsp;</p>
<p><strong>Introduzione</strong>:</p>
<p>Nella giornata di <strong>Marted&igrave; 12 Aprile 2016</strong>, &egrave; prevista una giornata di testing.</p>
<p>La pagina relativa all&#39;evento &egrave; la seguente: <a href="https://fedoraproject.org/wiki/Test_Day:2016-04-12_I18N_Test_Day">https://fedoraproject.org/wiki/Test_Day:2016-04-12_I18N_Test_Day</a></p>
<p>I risultati devono essere inseriti separatamente: <a href="http://testdays.fedorainfracloud.org/events/4">http://testdays.fedorainfracloud.org/events/4</a></p>
<p>&nbsp;</p>
<p><strong>Preparazione del sistema</strong>:</p>
<ul>
	<li>
		<p>Occorre avere una (o pi&ugrave;) tastiere con layout non americano.</p>
	</li>
	<li>
		<p>Installazione fisica, guest in virtual machine o live USB di un&#39;immagine di Fedora. Possono essere usate le seguenti ISO:</p>
		<ol class="decimal">
			<li>
				<p>Fedora 24 Alpha</p>
			</li>
			<li>
				<p>Fedora 24 Nightly Live Build recente (scaricabile da <a href="https://www.happyassassin.net/nightlies.html">https://www.happyassassin.net/nightlies.html</a>)</p>
			</li>
		</ol>
	</li>
	<li>
		<p>Prima di eseguire i test, &egrave; necessario <strong>aggiornare il sistema</strong></p>
	</li>
</ul>
<p>&nbsp;</p>
<p><strong>Casi da testare</strong>:</p>
<p>La pagina dei risultati - descritta nel paragrafo &quot;Introduzione&quot; - contiene numerosi link (ibus, applications, browser, dnf-langpacks ecc.) con i passi da seguire.</p>
<p>&nbsp;</p>
<p><strong>IRC</strong>:</p>
<p>Il canale internazionale ufficiale per l&#39;evento &egrave; il consueto #fedora-test-day.</p>
