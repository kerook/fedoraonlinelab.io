---
categories:
- staff news
layout: ultime_news
title: 'Test Days F23: Cinnamon'
created: 1443548772
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p class="rtejustify"><br />
	Dopo alcuni rinvii, &egrave; stata resa nota la data relativa al <a href="http://doc.fedoraonline.it/TestDays">Test Day</a> della nuova spin <a href="https://fedoraproject.org/wiki/Test_Day:2015-10-04_Cinnamon">Cinnamon</a>, che si terr&agrave; domenica 04 Ottobre 2015.<br />
	<br />
	<strong>Preparazione del sistema</strong>:<br />
	&Egrave; necessario utilizzare un computer con una scheda grafica capace di supportare OpenGL. Per questo specifico evento, non si potr&agrave; quindi usufruire di una macchina virtuale.<br />
	Con l&#39;avvicinarsi della data interessata, verr&agrave; reso disponibile un apposito file ISO per la creazione di una live USB.<br />
	Alternativamente, sar&agrave; possibile installare fisicamente la beta di Fedora 23 Cinnamon, avendo per&ograve; cura di applicare tutti gli aggiornamenti disponibili prima di eseguire le prove.<br />
	<br />
	<strong>Casi da testare</strong>:<br />
	I casi da testare sono elencati nella <a href="https://fedoraproject.org/wiki/Test_Day:2015-10-04_Cinnamon">pagina dedicata alla giornata</a>, la quale contiene anche una tabella per riepilogare i risultati.<br />
	Stabilit&agrave; del browser, funzionamento del terminale, e test dell&#39;audio sono alcuni esempi di verifiche da effettuare.<br />
	Va ricordato poi che, anche se i passaggi da seguire sono ben precisi e ben definiti, qualsiasi tipo di prova &egrave; la benvenuta.<br />
	<br />
	<strong>IRC</strong>:<br />
	Come di consueto,&nbsp; il canale ufficiale sar&agrave; <a href="irc://irc.freenode.net/#fedora-test-day">#fedora-test-day</a></p>
