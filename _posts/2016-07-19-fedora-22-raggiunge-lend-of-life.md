---
categories:
- staff news
layout: ultime_news
title: Fedora 22 raggiunge l'End Of Life!
created: 1468956399
---
<p><img alt="" src="https://cdn.fedoramagazine.org/wp-content/uploads/2016/06/fedora22eol-945x400.png" style="width: 399px; height: 169px;" /></p>
<p class="rtejustify">&nbsp;</p>
<p class="rtejustify">Oggi (19 Luglio 2016), il rilascio 22 di Fedora ha raggiunto l&#39;<strong>End Of Life</strong>.</p>
<p class="rtejustify">Questo implica che:</p>
<ul>
	<li class="rtejustify">
		I pacchetti nei repository di Fedora 22, non riceveranno pi&ugrave; aggiornamenti (neanche quelli pi&ugrave; importanti, relativi alla sicurezza).</li>
	<li class="rtejustify">
		Nessun nuovo software verr&agrave; aggiunto ai repository di Fedora 22.</li>
	<li class="rtejustify">
		&Egrave; consigliato effettuare un upgrade verso una versione attivamente mantenuta (Fedora 23 o Fedora 24).</li>
</ul>
<p class="rtejustify">Per quanto riguarda il forum di Fedora Online, si assister&agrave; prossimamente alla chiusura della sezione relativa a Fedora 22. Le discussioni esistenti, confluiranno nella sezione &quot;[EOL]&quot;.</p>
