---
categories:
- staff news
layout: ultime_news
title: '"Open House" IRC Meeting - Giovedì 23/02 ore 19'
created: 1329817063
---
<p>
	<span>Tom Callaway, </span><span>Fedora Engineering Manager, annuncia che per <strong>gioved&igrave; 23 febbraio 2012 alle ore 19:00</strong> &egrave; previsto un meeting IRC</span><span> &quot;Open House&quot;.</span> Sar&agrave; un meeting moderato, in cui verranno presentati i progetti in fase di sviluppo del <span>Fedora Engineering team</span> e in cui si raccoglieranno le domande, i suggerimenti e i feedback da parte della comunit&agrave; Fedora.</p>
<p>
	Il meeting avr&agrave; luogo nel solito canale IRC: <span>#fedora-meeting su <a href="http://irc.freenode.net/" target="_blank">irc.freenode.net</a> e sar&agrave; logged.</span></p>
<p>
	Il protocollo del meeting &egrave; disponibile qui:<span> <a href="https://fedoraproject.org/wiki/How_to_use_IRC#Meeting_Protocol" target="_blank">https://fedoraproject.org/wiki/How_to_use_IRC#Meeting_Protocol</a></span><br />
	<br />
	<span>Per chi non sapesse chi o cos&#39;&egrave; il Fedora Engineering, in sintesi si pu&ograve; descrivere come una squadra creata da Redhat, che lavora a tempo pieno cercando di migliorare Fedora e assicurando cos&igrave; la robustezza e l&#39;infrastruttura della distribuzione.</span><br />
	<br />
	<span>Una risposta pi&ugrave; dettagliata si trova qui: </span><span><a href="https://fedoraproject.org/wiki/Fedora_Engineering" target="_blank">https://fedoraproject.org/wiki/Fedora_Engineering</a></span><br />
	<br />
	<span>I progetti proposti e gli obiettivi per il prossimo anno sono documentati qui: </span><span><a href="https://fedoraproject.org/wiki/Fedora_Engineering/FY13_Plan" target="_blank">https://fedoraproject.org/wiki/Fedora_Engineering/FY13_Plan</a></span><br />
	<br />
	Nel pieno spirito della trasparenza e della comunit&agrave;, il team incoraggia tutti i membri delle comunit&agrave; Fedora (che siano essi utenti, tester, redattori, grafici, traduttori, ambassador, packager o qualsiasi altro tipo di contributor) di partecipare e di incontrare il componenti del team per conoscere i piani per l&#39;anno prossimo.</p>
<p>
	<span>Fonte ML: </span><span><a>announce@lists.fedoraproject.org</a></span></p>
