---
categories:
- staff news
layout: ultime_news
title: 'Fedora 19 Beta è pronta: rilascio il 28 maggio!'
created: 1369344568
---
<p>Stavolta niente rimando, niente dubbi, niente di niente. Solo: GO!</p>
<p>Questo, in sintesi, &egrave; il risultato del meeting di oggi sul rilascio o meno della Beta di Fedora 19. Pochi giorni quindi e potremo passare al beta-testing, e vista la bont&agrave; della Alpha la beta potrebbe veramente essere gi&agrave; a un buon punto. Vi ricordo che per passare alla Beta si potr&agrave; utilizzare anche fedup, cosa che non si poteva usare invece per passare alla Alpha.</p>
<p>Per l&#39;annuncio e il log del meeting vi rimando all&#39;annuncio ufficiale:</p>
<p><a href="http://lists.fedoraproject.org/pipermail/devel/2013-May/183299.html">http://lists.fedoraproject.org/pipermail/devel/2013-May/183299.html</a></p>
<p>Buon test a tutti!</p>
