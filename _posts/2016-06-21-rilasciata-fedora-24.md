---
categories:
- staff news
layout: ultime_news
title: Rilasciata Fedora 24!!!
created: 1466538906
---
<p><img alt="" src="https://cdn.fedoramagazine.org/wp-content/uploads/2016/06/fedora24release-945x400.jpg" style="width: 399px; height: 169px;" /></p>
<p>Nel corso della giornata di oggi, &egrave; stata rilasciata la versione 24 di Fedora.</p>
<p>I link per il download, sono i seguenti:<br />
	- <a href="https://getfedora.org/workstation/">https://getfedora.org/workstation/</a><br />
	- <a href="https://getfedora.org/server/">https://getfedora.org/server/</a><br />
	- <a href="https://getfedora.org/cloud/">https://getfedora.org/cloud/</a><br />
	- <a href="https://spins.fedoraproject.org/">https://spins.fedoraproject.org/</a><br />
	- <a href="https://labs.fedoraproject.org/">https://labs.fedoraproject.org/</a><br />
	- <a href="https://arm.fedoraproject.org/">https://arm.fedoraproject.org/</a><br />
	<br />
	La wiki di Fedora Online contiene alcune utili guide per procedere all&#39;installazione o all&#39;avanzamento di versione:<br />
	- Nuova installazione (<a href="http://doc.fedoraonline.it/Installazione_Fedora_22">http://doc.fedoraonline.it/Installazione_Fedora_22</a>) (<a href="http://doc.fedoraonline.it/Installazione_Fedora">http://doc.fedoraonline.it/Installazione_Fedora</a>)<br />
	- Upgrade da rilasci precedenti (<a href="http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade">http://doc.fedoraonline.it/Aggiornamento_Fedora_con_system-upgrade</a>)<br />
	<br />
	Di seguito si esaminano alcune delle principali novit&agrave;, che accompagnano il nuovo rilascio.<br />
	<br />
	<strong>Worskstation</strong><br />
	- Migliorato lo strumento di ricerca all&#39;interno di &quot;Files (Nautilus)&quot;.<br />
	- Introduzione di una nuova voce di men&ugrave; che riepiloga le scorciatoie da tastiera (per le applicazioni che supportano questa nuova funzionalit&agrave;).<br />
	- Apportati cambiamenti alle impostazioni relative a dispositivi di input e di stampa.<br />
	- Introduzione di &quot;Fedora Media Writer&quot;, che sostituisce &quot;Live Usb Creator&quot;, per la preparazione di dispositivi live.<br />
	- &quot;Software&quot; elenca le recensioni create dagli utenti.<br />
	- Approntati nuovi controlli per l&#39;editing di foto all&#39;interno di &quot;Photos&quot;.<br />
	- &quot;Python 3&quot; &egrave; stato aggiornato alla versione 3.5.<br />
	- Il nuovo &quot;Darktable&quot; (versione 2) &egrave; facilmente reperibile dai repository standard.<br />
	- &quot;Documents&quot; consente di elencare i file per autore, data o nome.<br />
	- &quot;Software&quot; prevede l&#39;avanzamento di versione. Questa novit&agrave; &egrave; ancora in fase embrionale e verr&agrave; perfezionata nelle prossime settimane.<br />
	- &quot;Wayland&quot; &egrave; presente come alternativa, ma non &egrave; stato ritenuto sufficientemente maturo per modificare la sessione predefinita di Fedora 24.<br />
	<br />
	&Egrave; stato realizzato un videoclip amatoriale, dedicato a Fedora 24 Beta, che mostra alcuni di questi miglioramenti: <a href="https://www.youtube.com/watch?v=4r851XnmTSA.">https://www.youtube.com/watch?v=4r851XnmTSA.</a><br />
	<br />
	<br />
	<strong>Server</strong><br />
	&quot;FreeIPA&quot; &egrave; una soluzione integrata per la gestione di informazioni dedicate alla sicurezza. Fedora 24 Server contiene la versione 4.3, che evidenzia semplificazioni e miglioramenti nella gestione di installazioni replicate.<br />
	<br />
	<strong>Cloud</strong><br />
	&quot;OpenShift Origin&quot; &egrave; un PAAS ottimizzato per lo sviluppo e il deploy. Questa nuova versione, pacchettizzata e disponibile in Fedora 24 Cloud, semplifica il lavoro di programmatori che desiderano realizzare le proprie applicazioni, sfruttando le nuove tecnologie dei container. &Egrave; presente una nuova infrastruttura per la realizzazione di software che si appoggia sull&#39;immagine Docker di Fedora. &Egrave; previsto un servizio di build pi&ugrave; completo, che verr&agrave; presumibilmente incluso e migliorato in tempo per i prossimi rilasci di Fedora.<br />
	<br />
	<strong>Atomic</strong><br />
	L&#39;edizione Atomic mantiene il proprio peculiare ciclo di rilascio, che rende disponibile una nuova immagine all&#39;incirca ogni due settimane. Da qualche mese, essa racchiude la modalit&agrave; &quot;developer mode&quot;, che sfrutta le potenzialit&agrave; di &quot;Cockpit&quot; e &quot;tmux&quot; per migliorare l&#39;esperienza utente degli sviluppatori.<br />
	<br />
	<strong>Spins</strong><br />
	Come sempre, gli utenti che preferiscono ambienti desktop alternativi, possono trovare immagini alternative. Fedora supporta in particolare KDE Plasma, ma &egrave; comunque presente un&#39;ottima variet&agrave; di spin (Xfce, LXDE, Mate-Compiz, Cinnamon, Sugar On A Stick).<br />
	<br />
	<strong>Labs</strong><br />
	I Fedora Labs, sono immagini che racchiudono un set personalizzato di applicazioni e sono dedicati a specifici classi di utilizzo. Accanto alle versioni &quot;tradizionali&quot; (Design Suite, Games, Robotics Suite, Scientific, Security Lab), Fedora 24 introduce una nuovit&agrave;. Il lab Astronomy infatti, include programmi scientifici (come Celestia, KStars, AstroPy, Virtualplanet, Astromatic, Gimp) dedicati agli appassionati dell&#39;osservazione e della fotografia astronomica.<br />
	<br />
	<strong>Bug comuni</strong><br />
	La wiki del Fedora Project, contiene, come di consueto, una pagina relativa ai bug comuni. Il link &egrave; il seguente: <a href="https://fedoraproject.org/wiki/Common_F24_bugs.">https://fedoraproject.org/wiki/Common_F24_bugs.</a><br />
	<br />
	<strong>Fonti e link ulteriori</strong><br />
	- Fedora Magazine: rilascio Fedora 24 (<a href="https://fedoramagazine.org/fedora-24-released/">https://fedoramagazine.org/fedora-24-released/</a>)<br />
	- Fedora Magazine: novit&agrave; Fedora Workstation (<a href="https://fedoramagazine.org/whats-new-fedora-24-workstation/">https://fedoramagazine.org/whats-new-fedora-24-workstation/</a>)<br />
	- Rilascio di Gnome 3.20 (<a href="https://www.gnome.org/news/2016/03/gnome-3-20-released/">https://www.gnome.org/news/2016/03/gnome-3-20-released/</a>)</p>
