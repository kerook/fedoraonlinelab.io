---
categories:
- staff news
layout: ultime_news
title: '13/10/2016: Wayland Test Day'
created: 1476338392
---
<p><img alt="" src="http://i816.photobucket.com/albums/zz84/bankotsu109/test-days-250px.png" style="width: 190px; height: 119px;" /></p>
<p>&nbsp;</p>
<p>Nella giornata di oggi, abbiamo in programma un importante Test Day.</p>
<p><br />
	<br />
	Wayland sar&agrave; infatti il server grafico di default per Fedora 25 Workstation. Il testing &egrave; iniziato gi&agrave; con la versione Alpha, ma si sarebbe importante raccogliere diversi dati sull&#39;utilizzo, per guadagnare maggiore confidenza prima del rilascio ufficiale.<br />
	<br />
	X11 rester&agrave; comunque come opzione: nei casi in cui esso &egrave; necessario, occorre assicurare che l&#39;utente abbia a disposizione questa scelta.<br />
	<br />
	Chiunque &egrave; in possesso di un computer, pu&ograve; partecipare e lasciare il proprio riscontro. Si prevede inoltre che le prove vengano effettuate con un&#39;immagine live, quindi un&#39;installazione fisica di Fedora 25 non si rende necessaria.</p>
<p><br />
	<br />
	La pagina wiki dell&#39;evento &egrave; disponibile al seguente link: <a href="https://fedoraproject.org/wiki/Test_Day:2016-10-13_Wayland">https://fedoraproject.org/wiki/Test_Day:2016-10-13_Wayland</a><br />
	&nbsp;</p>
