---
categories:
- staff news
layout: ultime_news
title: Sospensione Meeting IRC
created: 1453445924
---
<p><img alt="" src="https://scontent-cdg2-1.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/12369249_1039992026045829_6478223874346465556_n.png?oh=0d8accca51238b3d85ae8ad36b4b448b&amp;oe=570F35A6" style="width: 400px; height: 183px;" /></p>
<p>&nbsp;</p>
<p>A causa della scarsa partecipazione ai meeting periodici IRC sul canale # fedora-it, si &egrave; decisa la sospensione degli stessi.<br />
	<br />
	L&#39;attivit&agrave; si concentrer&agrave; eventualmente su incontri tematici pi&ugrave; specifici (presentazione nuovi rilasci, lezioni/FAD, ecc.)<br />
	<br />
	Di seguito il link alla discussione, aperta dall&#39;ambassador Frafra, all&#39;interno del forum di FedoraOnline: <a href="http://forum.fedoraonline.it/viewtopic.php?id=24183">http://forum.fedoraonline.it/viewtopic.php?id=24183</a></p>
