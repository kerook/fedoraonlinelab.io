---
categories: []
layout: page
title: A proposito di FOL
created: 1317498489
---
<h2>
	Fedora Online v2.1 (2012)</h2>
<ul>
	<li>
		Nuova sezione &quot;Planet Fol&quot;, una raccolta dei blog su Fedora in lingua italiana;</li>
	<li>
		Condivisione delle notizie del Planet su Facebook e Twitter;</li>
	<li>
		Pubblicazione del numero &quot;zero&quot; di Folio, il magazine di FedoraOnline.</li>
</ul>
<p>
	<img src="/sites/all/themes/newfol20/images/folio_00.jpg" /></p>
<hr />
<h2>
	Fedora Online v2.0 (2011)</h2>
<ul>
	<li>
		Nuovo layout in linea con il fedoraproject;</li>
	<li>
		Migrazione da Xoops a Drupal per quanto riguarda i contenuti statici e basilari;</li>
	<li>
		Migrazione del Forum a Fluxbb;</li>
	<li>
		Nuova gestione della documentazione attraverso Mediawiki e migrazione delle guide gi&agrave; presenti verso il nuovo wiki.</li>
</ul>
<p>
	<img src="/sites/all/themes/newfol20/images/fol20.jpg" /></p>
<hr />
<h2>
	Fedora Online v1.2 (2009)</h2>
<ul>
	<li>
		Aggiornamento dell&#39;intero core di xoops dalla versione 2.0.x alla 2.4.x con conseguente adattamento dei moduli presenti;</li>
	<li>
		Inserimento dei tab in prima pagina per una migliore gestione delle notizie.</li>
</ul>
<p>
	<img src="/sites/all/themes/newfol20/images/fol12.png" /></p>
<hr />
<h2>
	<strong>Fedora Online v1.1 (2006)</strong></h2>
<ul>
	<li>
		Nuovo logo e inserimento di nuove sezioni, come i download, gli aggiornamenti recenti o la creazione di una netiquette.</li>
</ul>
<hr />
<h2>
	<strong>Fedora Online v1.0 (17 gennaio 2005)</strong></h2>
<ul>
	<li>
		Nascita di fedoraonline.it basato su xoops 2.0.x.</li>
</ul>
<p>
	<img src="/sites/all/themes/newfol20/images/fol10.jpg" /></p>
