---
categories:
- staff news
layout: ultime_news
title: 'Via libera per Fedora 20 Beta: rilascio il 12 novembre!'
created: 1383851437
---
<p>Dopo due rimandi ecco la fumata bianca, Fedora 20 Beta verr&agrave; rilasciata marted&igrave; 12 novembre, rimangono alcuni punti interrogativi sul rilascio finale.</p>
<p>Infatti esiste un bug abbastanza importante sul ridimensionamento del disco (Anaconda), che per&ograve; non &egrave; stato ritenuto bloccante per il rilascio della versione Beta, lo sar&agrave; per&ograve; per il rilascio della versione finale. Inoltre, come da richiesta la settimana scorsa, FESCO ha approvato il fatto di anticipare il rilascio finale al 10 dicembre...quindi una situazione che &egrave; tutta da vedere, perch&egrave; non si conosce per adesso l&#39;entit&agrave; di questo bug. Ultimo ma non ultimo non esiste alcuna possibilit&agrave; di slittare una settimana dal 17 dicembre, se non andando nel nuovo anno, cosa che vorremmo tutti evitare. Che sia un Heisenbug?</p>
<p>Godiamoci intanto la Beta, con l&#39;obbligatorio rimando ai Common Bugs e alle Note di Rilascio:</p>
<p><a href="https://fedoraproject.org/wiki/Common_F20_bugs">https://fedoraproject.org/wiki/Common_F20_bugs</a></p>
<p><a href="http://fedoraproject.org/wiki/F20_Beta_release_announcement">http://fedoraproject.org/wiki/F20_Beta_release_announcement</a></p>
<p>Teniamo sott&#39;occhio quindi anche l&#39;evoluzione della Release schedule:</p>
<p><a href="https://fedoraproject.org/wiki/Releases/20/Schedule">https://fedoraproject.org/wiki/Releases/20/Schedule</a></p>
