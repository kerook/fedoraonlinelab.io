---
categories: []
layout: page
title: 'FOL: un po'' di storia'
created: 1317498190
---
<h2>Fedora Online - le origini</h2>

<p>L&#39;idea per Fedora Online nasce nel lontano 2004, con l&#39;obiettivo di raccogliere delle guide per principianti e di mettere a disposizione un piccolo forum per discutere delle prime difficolt&agrave; incontrate con il sistema operativo Fedora, allora ancora giovanissimo. Presto l&#39;idea si concretizza e il 17 gennaio 2005 fedoraonline.it va online. Complice il successo di Fedora come sistema operativo, gli utenti crescono e il forum diventa sempre pi&ugrave; un punto di ritrovo dove reperire informazioni, scambiare opinioni e leggere le guide.</p>

<p>A soli quattro mesi il team di fedoraonline.it non &egrave; pi&ugrave; composto dal solo webmaster, ma da due persone:</p>

<ul>
	<li>Robert Mayr (robyduck) - webmaster e amministratore contenuti</li>
	<li>gronbyt - moderatore del Forum (2005-2006)</li>
</ul>

<p>Dal 2006 invece, e fino al 2008, il sito &egrave; gestito da:</p>

<ul>
	<li>Robert Mayr (robyduck) - webmaster e amministratore contenuti</li>
	<li>Gabriele Trombini (mailga) - moderatore del Forum</li>
</ul>

<h2>Sviluppo e consolidamento</h2>

<p>Dal 2008 gli eventi si susseguono e la squadra intorno a FOL continua a crescere, come iniziano a crescere gli utenti. Dopo che un altro sito, fedoraserver.org, nel 2008 confluisce in Fedora Online, di fatto fedoraonline.it diventa l&#39;unica comunit&agrave; attiva in Italia e di conseguenza tratta gli argomenti ormai a 360&deg;.</p>

<p>Con l&#39;aggiornamento del cuore di FOL nel 2009 lo staff si allarga ulteriormente e comprende:</p>

<ul>
	<li>Robert Mayr (robyduck) - webmaster e amministratore contenuti</li>
	<li>Gabriele Trombini (mailga) - moderatore del Forum</li>
	<li>Giuseppe Del Vecchio (virus) - moderatore del Forum</li>
	<li>Mario Santagiuliana (MarioS) - amministratore tecnico</li>
	<li>Filippo Racca (Astharoth) - collaboratore (2009-2011)</li>
</ul>

<p>Tra il 2010 e il 2011 Fedora Online cerca di incentivare sempre di pi&ugrave; gli utenti a partecipare al Progetto Fedora. Vengono create sezioni apposite e di contro Fol con l&#39;uscita del portale internazionale delle comunit&agrave; Fedora, diventa il riferimento per l&#39;Italia.</p>

<p>Il sito da questo momento &egrave; raggiungibile anche attraverso l&#39;indirizzo http://it.fedoracommunity.org/</p>

<h2>Ristrutturazione</h2>

<p>Nel 2011 Fedora Online sviluppa un traffico di dati e un carico ai server notevole, e per continuare a dare supporto ai quasi 7000 iscritti, Xoops lascia spazio a una soluzione pi&ugrave; adatta e performante. FOL viene completamente riscritto e con l&#39;uscita della versione 2.0 lo staff &egrave; il seguente:</p>

<ul>
	<li>Robert Mayr (robyduck) - webmaster e amministratore contenuti</li>
	<li>Alessandro Gala (pagnolo) - amministratore</li>
	<li>Mario Santagiuliana (MarioS) - amministratore tecnico</li>
	<li>Gabriele Trombini (mailga) - moderatore del Forum/Direttore Folio</li>
	<li>Giuseppe Delvecchio (virus) - moderatore del Forum</li>
</ul>

<p>Collaborano per quanto riguarda i contenuti e i continui test sul sito:</p>

<ul>
	<li>Cristian Pozzessere (ilnanny) - grafica</li>
	<li>Luigi D&#39;Angelo (tuzzer) - grafica</li>
	<li>sevy72, silvio - news</li>
	<li>Simone Zaminga (zamingas) - test</li>
</ul>

<h2>Assestamento</h2>

<p>Con l&#39;uscita del Planet Fol, una raccolta di blog su Fedora in lingua italiana, e la decisione di utilizzare anche i due canali social network pi&ugrave; famosi, lo staff di Fol subisce ulteriori variazioni. Inoltre l&#39;uscita del &quot;numero zero&quot; del webmagazine Folio impone una strutturazione ancora pi&ugrave; attenta, sia per quanto riguarda la gestione dei contenuti, ma anche per facilitare agli utenti l&#39;accesso per contribuire alla nostra &quot;rivista&quot;.</p>

<p>Lo staff &egrave; quindi composto da:</p>

<ul>
	<li>Robert Mayr (robyduck) - webmaster e amministratore contenuti</li>
	<li>Alessandro Gala (pagnolo) - amministratore</li>
	<li>Mario Santagiuliana (MarioS) - amministratore tecnico</li>
	<li>Gabriele Trombini (mailga) - moderatore del Forum/Direttore Folio</li>
	<li>Giuseppe Delvecchio (virus) - moderatore del Forum</li>
	<li>Giuseppe Raveduto (wardialer) - moderatore del Forum</li>
</ul>

<p>Mentre i collaboratori, senza i quali molte sezioni non potrebbero esistere, d&#39;ora in poi sono:</p>

<ul>
	<li>Cristian Pozzessere (ilnanny) - grafica</li>
	<li>Luigi D&#39;Angelo (tuzzer) - grafica</li>
	<li>Antonio Gemelli (antowen) - gestione social network</li>
	<li>Gianluca Sforna (giallu) - Fedora Ambassador, organizzatore eventi</li>
</ul>

<h2>Dieci anni dopo</h2>

<p>Le persone cambiano, ma la storia di FOL continua. Ecco lo staff:</p>

<ul>
	<li>bebo_sudo - Documentazione</li>
	<li>Pocoto - Moderatore</li>
	<li>Arkanoid/juliuxpigface - Moderatore, redattore e social</li>
	<li>Yattattux - Redattore e social (in passato &egrave; stato anche moderatore)</li>
	<li>kitten - Social</li>
	<li>Frafra - Amministratore e Fedora Ambassador (succeduto a Fale nell&#39;ottobre del 2017)</li>
</ul>
