---
categories:
- staff news
layout: ultime_news
title: Nuove feature accettate per Fedora 19
created: 1360741566
---
<p>Abbiamo appena oltrepassato la data limite per proporre nuove feature per Fedora 19. Come sempre le proposte sono state tantissime e alcune discussioni molto animate, soprattutto su alcuni argomenti. Per chi fosse interessato di sapere quali delle feature sono state accettate dal FESCO&nbsp;per Fedora 19 pu&ograve; consultare:</p>
<p><a href="http://fedoraproject.org/wiki/Releases/19/FeatureList#Fedora_19_Accepted_Features">http://fedoraproject.org/wiki/Releases/19/FeatureList#Fedora_19_Accepted_Features</a></p>
<p>Tra le pi&ugrave; importanti troviamo MariaDB come DB di default, che quindi spodesta MySQL, Enlightenment, KScreen e Ruby 2.0.</p>
